# Aiyoga

#### 介绍
一款与ai联动的瑜伽软件 

#### 软件架构
软件架构说明




#### 未解决问题

1. 课程具体展示页面，上方是视频预览，播放几十秒的视频
2. 社区还未进行编写（6.18样式）
3. 评论的ui和数据获取都没进行，
4. 普通训练单纯播放课程，矫正训练动作视频跟练（初步思路：定时器获取一段时间，设置动作），收藏课程连接。
5. 搜索框搜索。(后端连接)
6. 帖子发布界面，对应类（图片，标题，内容，是否可见，地点，相关课程名称）,对应是否可见，地点，相关课程名称的界面未完成，提交到服务器的数据连接未完成



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
