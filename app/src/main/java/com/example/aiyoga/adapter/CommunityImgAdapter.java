package com.example.aiyoga.adapter;

import static org.litepal.LitePalApplication.getContext;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.CommunityUser;
import com.example.aiyoga.activity.main.CommunityFragment;

import java.util.List;

public class CommunityImgAdapter extends RecyclerView.Adapter<CommunityImgAdapter.ViewHolder>{
    private List<CommunityUser> mList;
    private Context mContext;
    public CommunityImgAdapter(List<CommunityUser> list,Context context){
        mList = list;
        mContext = context;
    }

    @NonNull
    @Override
    public CommunityImgAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_community, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommunityImgAdapter.ViewHolder holder, int position) {
        holder.mText.setText(mList.get(position).getUsername());
        Uri uri = Uri.parse(mList.get(position).getAvatar());
        if(uri.toString().equals("") || uri == null){
            Glide.with(mContext)
                    .load(R.drawable.ic_launcher_foreground)
                    .circleCrop()
                    .into(holder.mImage);
        } else {
            Glide.with(mContext)
                    .load(uri)
                    .circleCrop()
                    .into(holder.mImage);
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView mText;
        private ImageView mImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mText = itemView.findViewById(R.id.tv_community);
            mImage = itemView.findViewById(R.id.iv_community);
        }
    }
}
