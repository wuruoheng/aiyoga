package com.example.aiyoga.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.ItemCommunity;

import java.util.List;

public class CommunityAdapter extends RecyclerView.Adapter<CommunityAdapter.CommunityViewHolder> {
    private Context mContext;
    private List<ItemCommunity> mList;

    public CommunityAdapter(Context context, List<ItemCommunity> list) {
        mContext = context;
        mList = list;
    }


    @NonNull
    @Override
    public CommunityAdapter.CommunityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_user_community, null);
        return new CommunityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommunityViewHolder holder, int position) {

        holder.mText.setText(mList.get(position).getContent());
        Uri uri = Uri.parse(mList.get(position).getImgUrl());
        Glide.with(mContext)
                .load(uri)
                .into(holder.mImage);
        holder.mTime.setText(mList.get(position).getTime());
        holder.mName.setText(mList.get(position).getUsername());
        Glide.with(mContext)
                .load(mList.get(position).getUserImg())
                .circleCrop()
                .into(holder.mIcon);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class CommunityViewHolder extends RecyclerView.ViewHolder{
        private TextView mName;
        private ImageView mImage;
        private ImageView mIcon;
        private TextView mText;
        private TextView mTime;

        public CommunityViewHolder(@NonNull View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.tv_community_user);
            mIcon = itemView.findViewById(R.id.iv_community_img);
            mText = itemView.findViewById(R.id.tv_community_desc);
            mImage = itemView.findViewById(R.id.iv_community_imge);
            mTime = itemView.findViewById(R.id.tv_time);
        }
    }
}
