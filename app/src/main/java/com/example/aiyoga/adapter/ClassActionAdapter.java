package com.example.aiyoga.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aiyoga.R;
import com.example.aiyoga.activity.classes.ContentActivity;
import com.example.aiyoga.activity.enity.ClassAction;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.PreferenceManager;

import java.util.List;

public class ClassActionAdapter extends BaseAdapter {
    private Context context;
    private int layoutId;
    private List<ClassAction> goods;
    private PreferenceManager preferenceManager;

    public ClassActionAdapter(Context context, int layoutId, List<ClassAction> goods, PreferenceManager preferenceManager) {
        this.context = context;
        this.layoutId = layoutId;
        this.goods = goods;
        this.preferenceManager = preferenceManager;
    }



    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int position) {
        return goods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //获取布局文件对象
        convertView = LayoutInflater.from(context)
                .inflate(layoutId,null);
        //获取布局文件中的控件对象
        LinearLayout layout = convertView.findViewById(R.id.lv_class_action);
        TextView tvName = convertView.findViewById(R.id.tv_action_name);
        TextView tvLevel = convertView.findViewById(R.id.tv_action_level);
        TextView tvType = convertView.findViewById(R.id.tv_action_level2);
        ImageView avatar = convertView.findViewById(R.id.imageview_class_action);

        //获取数据源中当前i位置的元素
        ClassAction goo = goods.get(position);
        tvName.setText(goo.getName());
        tvLevel.setText(goo.getGrade()+"级");
        tvType.setText(goo.getType());
        if(goo.getImage() != null) {
            Glide.with(convertView).load(preferenceManager.getString(Constants.USER_AVATAR)).into(avatar);
        }

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转到详情页
                Intent intent = new Intent(context, ContentActivity.class);
                intent.putExtra("classde",goo.getId());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

}

