package com.example.aiyoga.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.PractiseListVO;

import java.util.List;

public class PersonTrainAdapter extends BaseAdapter {
    Context context;
    List<PractiseListVO> list;
    public PersonTrainAdapter(Context context, List<PractiseListVO> list) {
        this.context = context;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = View.inflate(context, R.layout.item_listexcise, null);

        TextView tvName = convertView.findViewById(R.id.tv_name);
        TextView tvTime = convertView.findViewById(R.id.tv_time);
        TextView tvCount = convertView.findViewById(R.id.tv_count);
        TextView tvCalorie = convertView.findViewById(R.id.tv_calorie);

        tvName.setText(list.get(position).getCourseName());
        tvTime.setText(list.get(position).getTime()+"");
        tvCount.setText(list.get(position).getScore()+"分");
        //tvCalorie.setText(list.get(position).getCalorie()+"千卡");


        return convertView;
    }
}
