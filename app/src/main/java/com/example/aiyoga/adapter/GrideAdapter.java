package com.example.aiyoga.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.classes.ClassItemActivity;
import com.example.aiyoga.activity.enity.Class;

import java.util.List;


public class GrideAdapter extends BaseAdapter {
    private Context context;
    private int layoutId;
    private List<Class> goods;

    public GrideAdapter(Context context, int layoutId, List<Class> goods) {
        this.context = context;
        this.layoutId = layoutId;
        this.goods = goods;
    }



    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int position) {
        return goods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //获取布局文件对象
        convertView = LayoutInflater.from(context)
                .inflate(layoutId,null);
        //获取布局文件中的控件对象
        TextView tvName = convertView.findViewById(R.id.item_class);

        //获取数据源中当前i位置的元素
        Class goo = goods.get(position);
        tvName.setText(goo.getClassName());

       /* tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转到详情页
                Intent intent = new Intent(context, ClassItemActivity.class);
                intent.putExtra("class",goo.getClassName().toString());
                context.startActivity(intent);
            }
        });*/
        return convertView;
    }

}
