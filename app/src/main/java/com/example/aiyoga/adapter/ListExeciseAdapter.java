package com.example.aiyoga.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.Class;
import com.example.aiyoga.activity.enity.PractiseListVO;

import java.util.List;

public class ListExeciseAdapter extends RecyclerView.Adapter<ListExeciseAdapter.MyViewHolder>{
    Context context;
    private List<PractiseListVO> practiseListVOS;
    public ListExeciseAdapter(Context context, List<PractiseListVO> practiseListVOS) {
        this.context = context;
        this.practiseListVOS = practiseListVOS;
    }

    public void setPractiseListVOS(List<PractiseListVO> practiseListVOList) {
        this.practiseListVOS = practiseListVOList;
    }


    @NonNull
    @Override
    public ListExeciseAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.item_listexcise, null);
        MyViewHolder myViewHoder = new MyViewHolder(view);
        return myViewHoder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListExeciseAdapter.MyViewHolder holder, int position) {
        PractiseListVO practiseListVO = practiseListVOS.get(position);
        holder.tvName.setText(practiseListVO.getCourseName());
        holder.tvTime.setText(practiseListVO.getTime()+"");
        holder.tvCount.setText(practiseListVO.getScore()+"分");
       //holder.tvCalorie.setText(practiseListVO.getCalorie());
    }

    @Override
    public int getItemCount() {
        return practiseListVOS.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tvName, tvTime,tvCount,tvCalorie;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvCount = itemView.findViewById(R.id.tv_count);
            tvCalorie = itemView.findViewById(R.id.tv_calorie);
        }
    }
}
