package com.example.aiyoga.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aiyoga.R;
import com.example.aiyoga.activity.classes.ClassItemActivity;
import com.example.aiyoga.activity.classes.ContentActivity;
import com.example.aiyoga.activity.enity.Class;
import com.example.aiyoga.activity.enity.ClassDetail;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.PreferenceManager;

import java.util.List;

public class ClassAdapter extends BaseAdapter {
    private Context context;
    private int layoutId;
    private List<ClassDetail> goods;
    private PreferenceManager preferenceManager;

    public ClassAdapter(Context context, int layoutId, List<ClassDetail> goods, PreferenceManager preferenceManager) {
        this.context = context;
        this.layoutId = layoutId;
        this.goods = goods;
        this.preferenceManager = preferenceManager;
    }



    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int position) {
        return goods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //获取布局文件对象
        convertView = LayoutInflater.from(context)
                .inflate(layoutId,null);
        //获取布局文件中的控件对象
        LinearLayout layout = convertView.findViewById(R.id.lv_class_details);
        TextView tvName = convertView.findViewById(R.id.textview_class_detail);
        TextView tvTime = convertView.findViewById(R.id.textview_class_time);
        ImageView avatar = convertView.findViewById(R.id.imageview_class_detail);

        //获取数据源中当前i位置的元素
        ClassDetail goo = goods.get(position);
        tvName.setText(goo.getName());
        if(goo.getTime() < 60000){
            tvTime.setText(goo.getTime()/1000+"秒");
        }else{
            if(goo.getTime()%60000/1000 != 0) {
                tvTime.setText(goo.getTime() / 60000 + "分钟" + goo.getTime() % 60000 / 1000 + "秒");
            } else {
                tvTime.setText(goo.getTime() / 60000 + "分钟");
            }
        }
        if(goo.getImage() != null) {
            Glide.with(convertView).load(goods.get(position).getImage()).into(avatar);
        }


        /*layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转到详情页
                Intent intent = new Intent(context, ContentActivity.class);
                intent.putExtra("id",goo.getId()+"");
                context.startActivity(intent);
            }
        });*/
        return convertView;
    }

}

