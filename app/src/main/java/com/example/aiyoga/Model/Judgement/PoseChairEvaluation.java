// chair第一个瑜伽动作
package com.example.aiyoga.Model.Judgement;


import java.util.Arrays;

public class PoseChairEvaluation {

    public static double Uv1(double x) {
        if (x <= 5) {
            return 1;
        } else if (5 < x && x <= 10) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 5 * (x - 7.5));
        } else if (x > 10) {
            return 0;
        }
        return 0;
    }

    public static double Uv2(double x) {
        if (x <= 5) {
            return 0;
        } else if (5 < x && x <= 10) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 5 * (x - 7.5));
        } else if (10 < x && x <= 15) {
            return 1;
        } else if (15 < x && x <= 35) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 20 * (x - 25));
        } else if (x > 35) {
            return 0;
        }
        return 0;
    }

    public static double Uv3(double x) {
        if (x <= 15) {
            return 0;
        } else if (15 < x && x <= 35) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 20 * (x - 25));
        } else if (35 < x && x <= 40) {
            return 1;
        } else if (40 < x && x <= 50) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 10 * (x - 45));
        } else if (x > 50) {
            return 0;
        }
        return 0;
    }

    public static double Uv4(double x) {
        if (x <= 40) {
            return 0;
        } else if (40 < x && x <= 50) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 10 * (x - 45));
        } else if (50 < x && x <= 55) {
            return 1;
        } else if (55 < x && x <= 70) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 15 * (x - 62.5));
        } else if (x > 70) {
            return 0;
        }
        return 0;
    }

    public static double Uv5(double x) {
        if (x <= 55) {
            return 0;
        } else if (55 < x && x <= 70) {
            return 0.5 - 0.5 * Math.sin(Math.PI / 15 * (x - 62.5));
        } else if (x > 70) {
            return 0;
        }
        return 0;
    }


    public static double evaluatePose(double u1, double u2, double u3, double u4, double u5, double u6, double u7, double u8,int a) {
        double[] U = {Math.abs(170 - u1), Math.abs(170 - u2), Math.abs(170 - u3), Math.abs(170 - u4), Math.abs(100 - u5), Math.abs(100 - u6), Math.abs(100 - u7), Math.abs(100 - u8)};

        double[][] R = new double[U.length][5];
        for (int i = 0; i < U.length; i++) {
            R[i][0] = Uv1(U[i]);
            // Calculate R[i][1], R[i][2], R[i][3], and R[i][4] using Uv2, Uv3, Uv4, and Uv5 functions
        }

        double[] A = {0.028, 0.028, 0.053, 0.053, 0.264, 0.264, 0.155, 0.155};
        double[] B = new double[5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < U.length; j++) {
                B[i] += A[j] * R[j][i];
            }
        }

        double sum = Arrays.stream(B).sum();
        for (int i = 0; i < 5; i++) {
            B[i] /= sum;
        }

        double[] V = {1, 0.8, 0.6, 0.3, 0};
        double score = 0;
        for (int i = 0; i < 5; i++) {
            score += V[i] * B[i];
        }
        score *= 100;

        return score;
    }



}
