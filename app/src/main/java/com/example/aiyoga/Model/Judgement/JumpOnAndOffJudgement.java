package com.example.aiyoga.Model.Judgement;


import com.example.aiyoga.Model.PoseLandMark;

public class JumpOnAndOffJudgement extends Judgement {
    PoseWarriorEvaluation poseWarriorEvaluation;
    @Override
    public float countTheNum() {
        PoseLandMark right_wrist = poseMarkers.get(15);
        PoseLandMark right_elbow = poseMarkers.get(13);
        PoseLandMark right_shoulder = poseMarkers.get(11);
        PoseLandMark right_hip = poseMarkers.get(23);
        PoseLandMark right_knee = poseMarkers.get(25);
        PoseLandMark right_ankle = poseMarkers.get(27);
        PoseLandMark left_wrist = poseMarkers.get(16);
        PoseLandMark left_elbow = poseMarkers.get(14);
        PoseLandMark left_shoulder = poseMarkers.get(12);
        PoseLandMark left_hip = poseMarkers.get(24);
        PoseLandMark left_knee = poseMarkers.get(26);
        PoseLandMark left_ankle = poseMarkers.get(28);
        double u1 = getAngle(left_shoulder, left_elbow, left_wrist);
        double u2 = getAngle(right_shoulder, right_elbow, right_wrist);
        double u3 = getAngle(left_elbow, left_shoulder, left_hip);
        double u4 = getAngle(right_elbow, right_shoulder, right_hip);
        double u5 = getAngle(left_shoulder, left_hip, left_knee);
        double u6 = getAngle(right_shoulder, right_hip, right_knee);
        double u7 = getAngle(left_hip, left_knee, left_ankle);
        double u8 = getAngle(right_hip, right_knee, right_ankle);

        double score = poseWarriorEvaluation.evaluatePose(u1, u2, u3, u4, u5, u6, u7, u8);
        return (float) score;

    }
}

