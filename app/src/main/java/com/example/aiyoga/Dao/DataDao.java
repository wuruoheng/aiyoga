package com.example.aiyoga.Dao;



import com.example.aiyoga.Model.Data;

import org.litepal.LitePal;

import java.util.List;

public class DataDao {



    public List<Data> findAll(){
        List<Data> dataList = LitePal.findAll(Data.class);
        return dataList;
    }

    public void deleteAll(){
        LitePal.deleteAll(Data.class);
    }

}
