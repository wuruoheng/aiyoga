package com.example.aiyoga.activity.enity;

import java.time.LocalDateTime;

/**
 * 瑜伽课程评论
 */
public class YogaCourseComment {
    //@ApiModelProperty("评论id")
    private Integer id;

    //@ApiModelProperty("用户id")
    private Integer userId;

   // @ApiModelProperty("用户头像")
    private String avatar;

   // @ApiModelProperty("用户昵称")
    private String name;

    //@ApiModelProperty("评论内容")
    private String comment;

   // @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
}
