package com.example.aiyoga.activity.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.Result;
import com.example.aiyoga.activity.enity.User;
import com.example.aiyoga.activity.enity.UserLoginMsg;
import com.example.aiyoga.activity.main.CenActivity;
import com.example.aiyoga.activity.main.FirstFragment;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.OkHttpUtils;
import com.example.aiyoga.utils.PreferenceManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class LoginInFragment extends Fragment {
    private EditText loginUser, loginPassword;
    private Button loginBtn;
    String s;

    String m;

    User user = new User();

    private MyListener myListener;

    private PreferenceManager preferenceManager;
    @SuppressLint("MissingInflatedId")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View page=  inflater.inflate(R.layout.fragment_login_in,null);
        preferenceManager = new PreferenceManager(getActivity().getApplicationContext());
        loginBtn = page.findViewById(R.id.login_button);
        loginUser = page.findViewById(R.id.login_user);
        loginPassword = page.findViewById(R.id.login_pwd);
        setListener();
        return page;
    }

    private void setListener() {
        myListener = new MyListener();
        loginBtn.setOnClickListener(myListener);

    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what != 1){
                Toast.makeText(getActivity(), m, Toast.LENGTH_SHORT).show();
            }
        }
    };


    class MyListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.login_button:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                int i = init();
                                Message message = new Message();
                                message.what = i;
                                handler.sendMessage(message);
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    break;
            }
        }

        private int init() {
            user.setPhone(loginUser.getText().toString());
            user.setPassword(loginPassword.getText().toString());
            Gson gson = new Gson();
            String json = gson.toJson(user);
//            //todo 登录对象将json传给服务器
//            //http://10.7.88.219:8080/user/login
            s = OkHttpUtils.doPost("/user/login", json,"");
            Result<UserLoginMsg> result =  gson.fromJson(s,new TypeToken<Result<UserLoginMsg>>(){}.getType());
            if(result.getCode()==1){
                //将登录信息保存到本地
                UserLoginMsg userLoginMsg = result.getData();
                Log.i("login",userLoginMsg.toString());
                preferenceManager.putString(Constants.USER_TOKEN, userLoginMsg.getToken());
                preferenceManager.putString(Constants.USER_ACCOUNT, userLoginMsg.getAccount());
                preferenceManager.putString(Constants.USER_AVATAR, userLoginMsg.getAvatar());
                preferenceManager.putString(Constants.USER_GENDER, userLoginMsg.getGender());
                preferenceManager.putString(Constants.USER_NAME, userLoginMsg.getName());

                Intent intent = new Intent();
                intent.setClass(getContext(), CenActivity.class);
                intent.putExtra("userName",userLoginMsg.getName());
                intent.putExtra("userAvatar",userLoginMsg.getAvatar());
                intent.putExtra("userGender",userLoginMsg.getGender());
                intent.putExtra("userAccount",userLoginMsg.getAccount());
                startActivity(intent);


                Log.d("LoginInFragment", s);
            } else {
                Log.d("LoginInFragment", s);

            }
            m = result.getMsg();
            return result.getCode();

        }
    }
}
