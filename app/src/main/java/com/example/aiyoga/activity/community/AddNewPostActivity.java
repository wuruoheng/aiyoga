package com.example.aiyoga.activity.community;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.aiyoga.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class AddNewPostActivity extends AppCompatActivity implements CustomBottomSheetDialog.OnPrivacySelectedListener{
    ImageView addReturn,addPicture;
    LinearLayout layoutExercise,layoutWhere,layoutPrivate;
    TextView PraviteTx;
    EditText addTitle,addContent;
    Button addCommit;
    //是一个全局常量，用于标识这是选择图片的这个操作
    public static final int CHOOSE_PHOTO = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_post);
        findViews();
        setListener();
    }

    private void setListener() {
        MyOnClickListener listener = new MyOnClickListener();
        addReturn.setOnClickListener(listener);
        addPicture.setOnClickListener(listener);
        addCommit.setOnClickListener(listener);
        layoutExercise.setOnClickListener(listener);
        layoutWhere.setOnClickListener(listener);
        layoutPrivate.setOnClickListener(listener);
    }

    private void findViews() {
        addReturn = findViewById(R.id.add_new_return);
        addPicture = findViewById(R.id.add_new_picture);
        layoutExercise = findViewById(R.id.linear_two);
        layoutWhere = findViewById(R.id.linear_three);
        layoutPrivate = findViewById(R.id.linear_four);
        addTitle = findViewById(R.id.add_new_title);
        addContent = findViewById(R.id.add_new_content);
        addCommit = findViewById(R.id.btn_commit);
        PraviteTx = findViewById(R.id.pravite_or_not_tx);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }

    @Override
    public void onPrivacySelected(String privacy) {
        PraviteTx.setText(privacy);
    }

    class MyOnClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.add_new_return:
                    finish();
                    break;
                case R.id.add_new_picture:
                    xzImage();//选择图片
                    break;
                case R.id.linear_two:
                    break;
                case R.id.linear_three:
                    break;
                case R.id.linear_four:
                    // 弹窗，包括公开或者私密两个按钮
                    CustomBottomSheetDialog bottomSheetDialog = new CustomBottomSheetDialog();
                    bottomSheetDialog.setOnPrivateSelectListener(AddNewPostActivity.this);
                    bottomSheetDialog.show(getSupportFragmentManager(), "CustomBottomSheetDialog");
                    break;
                case R.id.btn_commit:
                    break;
            }

        }
    }


    private void xzImage() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent,CHOOSE_PHOTO); // 打开本地存储
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //requestCode：标识码
        //data：选择的图片的信息
        switch (requestCode) {
            case CHOOSE_PHOTO:
                //显示图片
                addPicture.setImageURI(data.getData());//放在ImageView中呈现
                break;
            default:
                break;
        }
    }
}