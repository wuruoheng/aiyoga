package com.example.aiyoga.activity.main;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.PractiseListVO;
import com.example.aiyoga.activity.enity.Result;
import com.example.aiyoga.adapter.ListExeciseAdapter;
import com.example.aiyoga.utils.ApiUrl;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.OkHttpUtils;
import com.example.aiyoga.utils.PreferenceManager;
import com.example.aiyoga.utils.gson.GsonManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class ExerciseFragment extends Fragment {
    private LinearLayout layout;
    //    private MyListener myListener;
    private CalendarView calendarView;
    //训练记录集合
    private List<PractiseListVO> practiseListVOList;
    private PreferenceManager preferenceManager;
    private RecyclerView recyclerView;

    private Gson gson;
    ListExeciseAdapter listExeciseAdapter;



    public ExerciseFragment() {
        // Required empty public constructor
    }





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exercise, container, false);


        calendarView = view.findViewById(R.id.calendarView);
        recyclerView = view.findViewById(R.id.recyclerView);


        gson = GsonManager.get();
        preferenceManager = new PreferenceManager(getContext());


        initData();

        //日历选择监听
        calendarView.setOnCalendarSelectListener(new CalendarView.OnCalendarSelectListener() {

            @Override
            public void onCalendarOutOfRange(Calendar calendar) {

            }

            @Override
            public void onCalendarSelect(Calendar calendar, boolean isClick) {
                if (isClick) {
                    //点击
                    //initData(calendar);
                    //获取当日训练记录
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            StringBuilder date = new StringBuilder();
                            String str = calendar.toString();
                            date.append(calendar.getYear());
                            date.append("-" + str.substring(4, 6));
                            date.append("-" + str.substring(6, 8));
                            Log.i("i", "选择日期：" + date);
                            String jsonResult = OkHttpUtils.doGet(ApiUrl.GET_PRACTISE_LIST + "?date=" + date, preferenceManager.getString(Constants.USER_TOKEN));
                            Result<List<PractiseListVO>> result = gson.fromJson(jsonResult, new TypeToken<Result<List<PractiseListVO>>>(){}.getType());
                            Log.i("i", "获取当日练习记录：" + result);
                            if (result.getCode() == 1) {
                                if (result.getData() == null) {
                                    practiseListVOList = new ArrayList<>();
                                } else {
                                    practiseListVOList = result.getData();
                                }
                                //TODO 更新下方列表
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        listExeciseAdapter.setPractiseListVOS(practiseListVOList);
                                        listExeciseAdapter.notifyDataSetChanged();
                                    }
                                });

                            }
                        }
                    }).start();
                }

            }
        });


        return view;
    }

    private Calendar getSchemeCalendar(int year, int month, int day, int color, String text) {
        Calendar calendar = new Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);
        calendar.setSchemeColor(color);//如果单独标记颜色、则会使用这个颜色
        calendar.setScheme(text);
        return calendar;
    }
    private void initData() {
        //获取本月训练记录并标记日历
        new Thread(new Runnable() {
            @Override
            public void run() {
                String jsonResult = OkHttpUtils.doGet(ApiUrl.GET_PRACTISE_LIST, preferenceManager.getString(Constants.USER_TOKEN));
                Log.i("i", jsonResult);
                Result<List<PractiseListVO>> result = gson.fromJson(jsonResult, new TypeToken<Result<List<PractiseListVO>>>(){}.getType());
                if (result.getCode() == 1) {
                    practiseListVOList = result.getData();
                    Log.i("i", practiseListVOList.toString());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //将有记录的日期颜色改变
                            /*Calendar schemeCalendar = getSchemeCalendar(2024, 6, 16, R.color.brown, "练习");
                            calendarView.addSchemeDate(schemeCalendar);*/
                            for (PractiseListVO practiseListVO : practiseListVOList) {
                                LocalDate trainingDate = practiseListVO.getTrainingDate();
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    Calendar schemeCalendar = getSchemeCalendar(trainingDate.getYear(), trainingDate.getMonthValue(), trainingDate.getDayOfMonth(), R.color.brown, "练习");
                                    calendarView.addSchemeDate(schemeCalendar);
                                }
                            }
                            //TODO 更新记录列表显示
                            if (listExeciseAdapter == null) {
                                listExeciseAdapter = new ListExeciseAdapter(getActivity(),practiseListVOList);
                                recyclerView.setAdapter(listExeciseAdapter);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(layoutManager);
                            } else {
                                listExeciseAdapter.setPractiseListVOS(practiseListVOList);
                                listExeciseAdapter.notifyDataSetChanged();
                            }

                        }
                    });
                }
            }
        }).start();



    }
}