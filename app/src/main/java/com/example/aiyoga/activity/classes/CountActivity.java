package com.example.aiyoga.activity.classes;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.main.CenActivity;

public class CountActivity extends AppCompatActivity {
    private CircleProgressView mCircleProgressView;
    private TextView mTextViewFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count);

        findViews();
        Intent intent = getIntent();
        Float count = intent.getFloatExtra("count",0);
        mCircleProgressView.setProgress(count);
    }

    private void findViews() {
        mCircleProgressView = findViewById(R.id.circle_progress_view);
        mTextViewFinish = findViewById(R.id.textView_finish);
    }
    private class MyListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.textView_finish:
                    Intent intent = new Intent(CountActivity.this, CenActivity.class);
                    intent.putExtra("id",mCircleProgressView.getProgress());
                    startActivity(intent);
                    break;
            }

        }
    }
}