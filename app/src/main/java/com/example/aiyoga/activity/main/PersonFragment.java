package com.example.aiyoga.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.aiyoga.R;
import com.example.aiyoga.activity.login.LoginActivity;
import com.example.aiyoga.activity.person.DataEditingActivity;
import com.example.aiyoga.activity.person.FeedbackActivity;
import com.example.aiyoga.activity.person.LoadRecordsActivity;
import com.example.aiyoga.activity.person.SettingActivity;
import com.example.aiyoga.activity.person.TrainRecordActivity;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.PreferenceManager;
import com.makeramen.roundedimageview.RoundedImageView;


public class PersonFragment extends Fragment {
    private RoundedImageView avatar;
    private LinearLayout layout;
    private LinearLayout mineTrainRecord, mineDataEditing, mineSetting,MineLoadRecords, MineFeedback;
    private MyListener myListener;
    private TextView mUserLogin, mUser;

    //    private MyListener myListener;
    private PreferenceManager preferenceManager;



    public PersonFragment() {
        // Required empty public constructor
    }





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mine, container, false);

        preferenceManager = new PreferenceManager(getContext());

        mineTrainRecord = view.findViewById(R.id.mine_train_records);
        mineDataEditing = view.findViewById(R.id.mine_data_editing);
        mineSetting = view.findViewById(R.id.mine_setting);
        MineLoadRecords = view.findViewById(R.id.mine_load_records);
        MineFeedback = view.findViewById(R.id.mine_feedback);

        mUserLogin = view.findViewById(R.id.m_user_login);
        mUser = view.findViewById(R.id.m_user_re);
        avatar = view.findViewById(R.id.avatar);

        //初始化用户信息
        initData();

        setOnClickListener();

        return view;
    }

    private void initData() {
        mUserLogin.setText(preferenceManager.getString(Constants.USER_NAME));
        mUser.setText(preferenceManager.getString(Constants.USER_ACCOUNT));
        Glide.with(this).load(preferenceManager.getString(Constants.USER_AVATAR)).into(avatar);
    }
    private void setOnClickListener() {
        myListener = new MyListener();
        mineTrainRecord.setOnClickListener(myListener);
        mineDataEditing.setOnClickListener(myListener);
        mineSetting.setOnClickListener(myListener);
        MineLoadRecords.setOnClickListener(myListener);
        MineFeedback.setOnClickListener(myListener);

        mUserLogin.setOnClickListener(myListener);
    }

    class MyListener implements View.OnClickListener {
        Intent intent;
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mine_train_records:
                    intent = new Intent(getContext(), TrainRecordActivity.class);
                    startActivity(intent);
                    break;
                case R.id.mine_data_editing:
                    intent = new Intent(getContext(), DataEditingActivity.class);
//                    intent.putExtra("user",name);
                    startActivity(intent);
                    break;
                case R.id.mine_setting:
                    intent = new Intent(getContext(), SettingActivity.class);
                    startActivity(intent);
                    break;
                case R.id.mine_load_records:
                    intent = new Intent(getContext(), LoadRecordsActivity.class);
                    startActivity(intent);
                    break;
                case R.id.mine_feedback:
                    intent = new Intent(getContext(), FeedbackActivity.class);
                    startActivity(intent);
                    break;
                case R.id.m_user_login:
                    intent = new Intent(getContext(), LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent.putExtra("flag", 1);
                    startActivity(intent);
                    break;
            }
        }
    }

}