package com.example.aiyoga.activity.main;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SearchView;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.community.AddNewPostActivity;
import com.example.aiyoga.activity.enity.CommunityUser;
import com.example.aiyoga.activity.enity.ItemCommunity;
import com.example.aiyoga.adapter.CommunityAdapter;
import com.example.aiyoga.adapter.CommunityImgAdapter;

import java.util.ArrayList;
import java.util.List;


public class CommunityFragment extends Fragment {
    private SearchView searchView;
    private String[] mStrs = {"aaa", "bbb", "ccc", "airsaid"};
    /**关注的人的列表，朋友圈的列表*/
    private RecyclerView recyclerView,mRecyclerView;
    /**
     * 添加新帖按钮*/
    private Button addNewPost;
    /**
    * 用户好友信息栏*/
    private List<CommunityUser> communityUserList = new ArrayList<>();

    /**
     * 帖子列表栏*/
    private List<ItemCommunity> list = new ArrayList<>();


    public static CommunityFragment newInstance(String param1, String param2) {
        CommunityFragment fragment = new CommunityFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_community, container, false);
//        searchView = view.findViewById(R.id.searchView);
        recyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView = view.findViewById(R.id.user_list);
        addNewPost = view.findViewById(R.id.add_newtitle_button);


        //加载社区好友用户数据
        initCommunityUserData();

        CommunityImgAdapter adapter = new CommunityImgAdapter(communityUserList,getActivity());
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        //横向滚动
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        CommunityAdapter communityAdapter = new CommunityAdapter(getActivity(),list);
        mRecyclerView.setAdapter(communityAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        setOnClickListener();


        // 设置搜索文本监听
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            // 当点击搜索按钮时触发该方法
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                //模糊查询
//                return false;
//            }
//
//            // 当搜索内容改变时触发该方法
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                if (!TextUtils.isEmpty(newText)){
//                    listView.setFilterText(newText);
//                }else{
//                    listView.clearTextFilter();
//                }
//                return false;
//            }
//        });
        return view;
    }

    private void setOnClickListener() {
        MyOnClickListener myOnClickListener = new MyOnClickListener();
        addNewPost.setOnClickListener(myOnClickListener);
    }

    private void initCommunityUserData() {
        CommunityUser communityUser = new CommunityUser("张三", "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg");
        communityUserList.add(communityUser);
        communityUser = new CommunityUser("李四", "");
        communityUserList.add(communityUser);
        communityUser = new CommunityUser("李四", "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg");
        communityUserList.add(communityUser);
        communityUser = new CommunityUser("李四", "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg");
        communityUserList.add(communityUser);
        communityUser = new CommunityUser("李四", "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg");
        communityUserList.add(communityUser);
        communityUser = new CommunityUser("李四", "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg");
        communityUserList.add(communityUser);
        communityUser = new CommunityUser("李四", "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg");
        communityUserList.add(communityUser);
        ItemCommunity itemCommunity = new ItemCommunity(
                "张三","今天天气不错，风好大，好热，好热，好热，好热，好热，好热，好热，好",
                "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg",
                "2020-11-18 12:00:00",
                "https://n.sinaimg.cn/sinakd10111/308/w1242h1466/20201118/7069-kcysmrw5281410.jpg");
        list.add(itemCommunity);
        list.add(itemCommunity);
        list.add(itemCommunity);
        list.add(itemCommunity);
    }

    class MyOnClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.add_newtitle_button:
                    Intent intent = new Intent(getActivity(), AddNewPostActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    }
}