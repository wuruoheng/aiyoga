package com.example.aiyoga.activity.person;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.Result;
import com.example.aiyoga.activity.enity.User;
import com.example.aiyoga.activity.main.CenActivity;
import com.example.aiyoga.utils.OkHttpUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Calendar;


public class DataEditingActivity extends AppCompatActivity {
    private EditText etEmail, etPhone,etBirthday;
    private Button btSave, btHeader;

    private TextView etUser;

    String m;

    private LinearLayout layout;
    String name,s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_editing);

        findViews();

        Intent intent = getIntent();
        if (intent != null) {
            name = intent.getStringExtra("user");
            etUser.setText(name);
        }

        setListener();
    }

    private void setListener() {
        DataEditingListener listener = new DataEditingListener();
        btSave.setOnClickListener(listener);
        etBirthday.setOnClickListener(listener);
    }

    private void findViews() {
        etUser = findViewById(R.id.data_user);
        etEmail = findViewById(R.id.data_email);
        etPhone = findViewById(R.id.data_phone);
        btSave = findViewById(R.id.data_save);
        btHeader = findViewById(R.id.data_update_header);
        layout = findViewById(R.id.data_cendal);
        etBirthday = findViewById(R.id.data_birthday);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what != 1){
                Toast.makeText(DataEditingActivity.this, "success", Toast.LENGTH_SHORT).show();
            }
        }
    };

    class DataEditingListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.data_save:
                    User user1 = new User();
                    user1.setName(name);
                    user1.setEmail(etEmail.getText().toString());
                    user1.setPhone(etPhone.getText().toString());
                    user1.setBirthday(etBirthday.getText().toString());

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                int i = init(user1);
                                Message message = new Message();
                                message.what = i;
                                handler.sendMessage(message);
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    break;
                case R.id.data_birthday:
                    Calendar calendar = Calendar.getInstance();
                    DatePickerDialog datePickerDialog = new DatePickerDialog(
                            DataEditingActivity.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            //monthOfYear 得到的月份会减1所以我们要加1
                            String time = String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + Integer.toString(dayOfMonth) + " ";
                            Log.d("测试", time);
                            etBirthday
                                    .setText(time);
                        }
                    },
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.show();
                    //自动弹出键盘问题解决
                    datePickerDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    break;
            }
        }
    }

    private int init(User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
//            //todo 登录对象将json传给服务器
        s = OkHttpUtils.doPut("/user/update", json,"");
        Result<User> result =  gson.fromJson(s,new TypeToken<Result<User>>(){}.getType());
        if(result.getCode()==200){
            Intent intent = new Intent();
            intent.setClass(DataEditingActivity.this, CenActivity.class);
            intent.putExtra("user",s);
            startActivity(intent);
            Log.d("DataEditingActivity", s);
        } else {
            Log.d("DataEditingActivity", s);

        }
        m = result.getMsg();
        return result.getCode();

    }



}