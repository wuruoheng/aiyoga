package com.example.aiyoga.activity.community;

import static org.litepal.LitePalApplication.getContext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.Result;
import com.example.aiyoga.activity.enity.UserLoginMsg;
import com.example.aiyoga.activity.main.CenActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**发帖仅谁可见/**/
public class ClassSelectActivity extends AppCompatActivity {
    private Button EveryOneBtn,PraBtn;

    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_select);

        findViews();

        setListener();

    }

    private void setListener() {
        MyOnClickListener myOnClickListener = new MyOnClickListener();
        EveryOneBtn.setOnClickListener(myOnClickListener);
        PraBtn.setOnClickListener(myOnClickListener);
    }

    private void findViews() {
        EveryOneBtn = findViewById(R.id.everyone_btn);
        PraBtn = findViewById(R.id.pravite_btn);
    }

    class MyOnClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.everyone_btn:

                    finish();
                    break;
                case R.id.pravite_btn:
                    finish();
                    break;
            }
        }
    }
    private int init() {

        Gson gson = new Gson();
//        String json = gson.toJson(user);
//            //todo 登录对象将json传给服务器
//            //http://10.7.88.219:8080/user/login
        //s = OkHttpUtils.doPost("/user/login", json,"");
        Result<UserLoginMsg> result =  gson.fromJson(s,new TypeToken<Result<UserLoginMsg>>(){}.getType());
        if(result.getCode()==1){
            //将登录信息保存到本地
            UserLoginMsg userLoginMsg = result.getData();
            Log.i("login",userLoginMsg.toString());
            //preferenceManager.putString(Constants.USER_TOKEN, userLoginMsg.getToken());


            Intent intent = new Intent();
            intent.setClass(getContext(), CenActivity.class);
            intent.putExtra("userName",userLoginMsg.getName());

            startActivity(intent);
        } else {
            //Log.d("LoginInFragment", s);

        }
        //m = result.getMsg();
        return result.getCode();

    }
}
