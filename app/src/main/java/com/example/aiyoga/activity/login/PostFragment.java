package com.example.aiyoga.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.Result;
import com.example.aiyoga.activity.enity.User;
import com.example.aiyoga.activity.enity.UserLoginMsg;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.OkHttpUtils;
import com.example.aiyoga.utils.PreferenceManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class PostFragment extends Fragment {
    private EditText postUser, postPassword,postEmail,postPhone,postPassword2;

    private Button postBtn;

    String m;

    private MyOnClickListener myOnClickListener;
    private PreferenceManager preferenceManager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post,null);
        preferenceManager = new PreferenceManager(getActivity().getApplicationContext());
        postUser = view.findViewById(R.id.post_user);
        postPassword = (EditText) view.findViewById(R.id.post_password);
        postEmail = view.findViewById(R.id.post_email);
        postPhone = view.findViewById(R.id.post_phone);
        postBtn = view.findViewById(R.id.post_button);
        postPassword2 = (EditText) view.findViewById(R.id.post_password2);

        setListener();

        return view;
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what != 1){
                Toast.makeText(getActivity(), m, Toast.LENGTH_SHORT).show();
            }
        }
    };
    private void setListener() {
        myOnClickListener = new MyOnClickListener();
        postBtn.setOnClickListener(myOnClickListener);
    }

    class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.post_button:
                    if(!postPassword.getText().toString().equals(postPassword2.getText().toString())){
                        Toast.makeText(getActivity(), "两次密码不一致", Toast.LENGTH_SHORT).show();
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                int i = initData();
                                Message message = new Message();
                                message.what = i;
                                handler.sendMessage(message);
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    break;
            }
        }
    }

    public int initData(){
        m = "";
        User user = new User();
        user.setName(postUser.getText().toString());
        user.setPassword(postPassword.getText().toString());
        user.setEmail(postEmail.getText().toString());
        user.setPhone(postPhone.getText().toString());
        Gson gson = new Gson();
        String json = gson.toJson(user);

        String s = OkHttpUtils.doPost("/user/register", json,"");
        Result<UserLoginMsg> result =  gson.fromJson(s,new TypeToken<Result<UserLoginMsg>>(){}.getType());
        if(result.getCode()==1){
            //将登录信息保存到本地
            UserLoginMsg userLoginMsg = result.getData();
            Log.i("login",userLoginMsg.toString());
            preferenceManager.putString(Constants.USER_TOKEN, userLoginMsg.getToken());
            preferenceManager.putString(Constants.USER_ACCOUNT, userLoginMsg.getAccount());
            preferenceManager.putString(Constants.USER_AVATAR, userLoginMsg.getAvatar());
            preferenceManager.putString(Constants.USER_GENDER, userLoginMsg.getGender());
            preferenceManager.putString(Constants.USER_NAME, userLoginMsg.getName());

            Intent intent = new Intent();
            intent.setClass(getContext(), LoginActivity.class);
            startActivity(intent);
        } else if(result.getCode()==0){
            m = result.getMsg();
        }

        return result.getCode();
    }
}
