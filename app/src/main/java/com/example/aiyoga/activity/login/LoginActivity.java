package com.example.aiyoga.activity.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.example.aiyoga.R;
import com.example.aiyoga.adapter.PageAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager2 viewPager2;
    private List<Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //获取布局文件中的控件对象
        tabLayout = findViewById(R.id.tb_l);
        viewPager2 = findViewById(R.id.vp_hm);
        //初始化数据源（子页面集合）
        initPages();
        //实例化对象
        PageAdapter adapter = new PageAdapter(fragments, this);
        //给ViewPager2绑定适配器
        viewPager2.setAdapter(adapter);
        //关联viewPage2和TabLayout
        TabLayoutMediator mediator = new TabLayoutMediator(
                tabLayout,
                viewPager2,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override
                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                        switch (position) {
                            case 0:
                                tab.setText("登录");
                                break;
                            case 1:
                                tab.setText("注册");
                                break;
                        }
                    }
                }
        );
        mediator.attach();

    }

    private void initPages() {
        fragments = new ArrayList<>();
        fragments.add(new LoginInFragment());
        fragments.add(new PostFragment());
    }



}