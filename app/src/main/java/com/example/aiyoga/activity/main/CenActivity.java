package com.example.aiyoga.activity.main;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.aiyoga.R;

import java.util.ArrayList;
import java.util.List;

public class CenActivity extends AppCompatActivity {
    private List<Fragment> fragmentList;
    private LinearLayout tab_home;
    private LinearLayout tab_mine,tab_exercise,tab_community;
    private ImageButton tab_home_img,tab_exercise_img,tab_community_img;
    private ImageButton tab_mine_img;
    private TextView tab_home_text,tab_exercise_text,tab_community_text;
    private TextView tab_mine_text;
    String userName,avatar,userGender,userAccount;

    private View.OnClickListener onClickListener = v -> {
        resetTabs();
        switch (v.getId()){
            case R.id.tab_home:
                selectTab(0);
                break;
            case R.id.tab_exercise:
                selectTab(1);
                break;
            case R.id.tab_community:
                selectTab(2);
                break;
            case R.id.tab_mine:
                selectTab(3);
                break;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //修改颜色
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
//        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#F3A3EA")));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cen);
        Intent intent = getIntent();
        userName = intent.getStringExtra("userName");
        avatar = intent.getStringExtra("userAvatar");
        userGender = intent.getStringExtra("userGender");
        userAccount = intent.getStringExtra("userAccount");
        initFragment();
        initViews();
        initEvents();
        initFirstRun(0);
    }

    private void initFirstRun(int i) {
        resetTabs();
        selectTab(0);
        setCurrentFragment(0);
    }

    private void initEvents() {
        tab_home.setOnClickListener(onClickListener);
        tab_mine.setOnClickListener(onClickListener);
        tab_exercise.setOnClickListener(onClickListener);
        tab_community.setOnClickListener(onClickListener);
    }

    private void initViews() {
        tab_home = findViewById(R.id.tab_home);
        tab_mine = findViewById(R.id.tab_mine);
        tab_home_img = findViewById(R.id.tab_home_img);
        tab_mine_img = findViewById(R.id.tab_mine_img);
        tab_home_text = findViewById(R.id.tab_home_text);
        tab_mine_text = findViewById(R.id.tab_mine_text);
        tab_exercise = findViewById(R.id.tab_exercise);
        tab_exercise_img = findViewById(R.id.tab_exercise_img);
        tab_exercise_text = findViewById(R.id.tab_exercise_text);
        tab_community = findViewById(R.id.tab_community);
        tab_community_img = findViewById(R.id.tab_community_img);
        tab_community_text = findViewById(R.id.tab_community_text);
    }

    private void initFragment() {
        fragmentList = new ArrayList<>();
        fragmentList.add(new FirstFragment());
        fragmentList.add(new ExerciseFragment());
        fragmentList.add(new CommunityFragment());
        fragmentList.add(new PersonFragment());
    }

    private void resetTabs() {
        tab_home_img.setImageResource(R.mipmap.img_home_unpressed);
        tab_mine_img.setImageResource(R.mipmap.mine_unpressed);
        tab_exercise_img.setImageResource(R.mipmap.img_execise_unpressed);
        tab_community_img.setImageResource(R.mipmap.chart_pie_outline);
        tab_home_text.setTextColor(getResources().getColor(R.color.gray));
        tab_mine_text.setTextColor(getResources().getColor(R.color.gray));
        tab_exercise_text.setTextColor(getResources().getColor(R.color.gray));
        tab_community_text.setTextColor(getResources().getColor(R.color.gray));
    }

    private void selectTab(int i) {
        switch (i){
            case 0:
                tab_home_img.setImageResource(R.mipmap.img_home_pressed);
                tab_home_text.setTextColor(getResources().getColor(R.color.white));
                Fragment fragment = fragmentList.get(i);
                Bundle bundle = new Bundle();
                bundle.putString("userName", userName);
                bundle.putString("userAvatar", avatar);
                bundle.putString("userGender", userGender);
                bundle.putString("userAccount", userAccount);
                fragment.setArguments(bundle);//数据传递到fragment中
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frag_layout,fragment);
                fragmentTransaction.commit();
                break;
            case 1:
                tab_exercise_img.setImageResource(R.mipmap.img_execise_pressed);
                tab_exercise_text.setTextColor(getResources().getColor(R.color.white));
                break;
            case 2:
                tab_community_img.setImageResource(R.mipmap.img_home_pressed);
                tab_community_text.setTextColor(getResources().getColor(R.color.white));
                break;
            case 3:
                tab_mine_img.setImageResource(R.mipmap.img_mine_pressed);
                tab_mine_text.setTextColor(getResources().getColor(R.color.white));
                break;
        }
        setCurrentFragment(i);
    }

    private void setCurrentFragment(int i) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction trans = fm.beginTransaction();
        trans.replace(R.id.frag_layout, fragmentList.get(i));
        trans.commit();
    }
}