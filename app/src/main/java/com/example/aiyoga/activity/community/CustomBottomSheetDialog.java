package com.example.aiyoga.activity.community;// BottomSheetDialogFragment 的实现
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.aiyoga.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.concurrent.atomic.AtomicInteger;

public class CustomBottomSheetDialog extends BottomSheetDialogFragment {
    private OnPrivacySelectedListener listener;
    public interface OnPrivacySelectedListener {
        void onPrivacySelected(String privacy);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottom_sheet_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // 设置按钮点击事件
        view.findViewById(R.id.button_cancel).setOnClickListener(v -> {
            dismiss(); // 关闭底部弹窗
        });

        view.findViewById(R.id.button_pravite).setOnClickListener(v -> {
            // 设置弹窗为私密
            if(listener != null){
                listener.onPrivacySelected("私密");
            }
            dismiss();
        });

        view.findViewById(R.id.button_public).setOnClickListener(v -> {
            // 设置弹窗为公开
            if(listener != null){
                listener.onPrivacySelected("公开");
            }
            dismiss();
        });
    }

    public void setOnPrivateSelectListener(OnPrivacySelectedListener listener) {
        this.listener = listener;
    }


}
