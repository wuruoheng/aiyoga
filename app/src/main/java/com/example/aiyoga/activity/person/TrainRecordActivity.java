package com.example.aiyoga.activity.person;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.PractiseListVO;
import com.example.aiyoga.adapter.PersonTrainAdapter;

import java.util.ArrayList;
import java.util.List;

public class TrainRecordActivity extends AppCompatActivity {
    private TextView txTrainTime, txTrainExcise, txTrainClass, txTrainDeplete;
    private ListView lvTrainRecord;
    List<PractiseListVO> practiseListVOS = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_record);

        findViews();
        
        initData();

        PersonTrainAdapter personTrainAdapter = new PersonTrainAdapter(this, practiseListVOS);
        lvTrainRecord.setAdapter(personTrainAdapter);
        
        getListener();
    }

    private void getListener() {
    }

    private void initData() {
        //todo 初始化数据，将数据放入其中
        for (int i = 0; i < 10; i++) {
            PractiseListVO practiseListVO = new PractiseListVO();
            practiseListVO.setTime(new Long(i));
            practiseListVO.setCourseName("11111");
            practiseListVO.setId(i);
            practiseListVOS.add(practiseListVO);
        }
    }

    private void findViews() {
        txTrainTime = findViewById(R.id.tx_train_time);
        txTrainExcise = findViewById(R.id.tx_train_execise);
        txTrainClass = findViewById(R.id.tx_train_class);
        txTrainDeplete = findViewById(R.id.tx_train_deplete);
        lvTrainRecord = findViewById(R.id.lv_train_record);
    }
}