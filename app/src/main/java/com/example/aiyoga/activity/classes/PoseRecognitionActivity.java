/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.aiyoga.activity.classes;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.example.aiyoga.Model.Judgement.Judgement;
import com.example.aiyoga.Model.Judgement.JudgementFactory;
import com.example.aiyoga.Model.Data;
import com.example.aiyoga.Model.PoseLandMark;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.YogaCourse;
import com.example.aiyoga.utils.Constants;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmark;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmarkList;
import com.google.mediapipe.framework.AndroidAssetUtil;
import com.google.mediapipe.framework.AndroidPacketCreator;
import com.google.mediapipe.framework.Packet;
import com.google.mediapipe.framework.PacketGetter;
import com.google.mediapipe.tasks.vision.core.RunningMode;
import com.google.mediapipe.tasks.vision.poselandmarker.PoseLandmarkerResult;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * PoseRecognition activity of MediaPipe example apps.
 */
public class PoseRecognitionActivity extends AppCompatActivity implements PoseLandmarkerHelper.LandmarkerListener{


    private static final String INPUT_NUM_HANDS_SIDE_PACKET_NAME = "num_hands";
    // Max number of hands to detect/process.
    private static final String INPUT_MODEL_COMPLEXITY = "model_complexity";

    private static final String TAG = "PoseRecognitionActivity";
    private static final String BINARY_GRAPH_NAME = "pose_tracking_gpu.binarypb";
    private static final String INPUT_VIDEO_STREAM_NAME = "input_video";
    private static final String OUTPUT_VIDEO_STREAM_NAME = "output_video";
    private static final String OUTPUT_LANDMARKS_STREAM_NAME = "pose_landmarks";
    private static final int NUM_HANDS = 2;

    // Flips the camera-preview frames vertically before sending them into FrameProcessor to be
    // processed in a MediaPipe graph, and flips the processed frames back when they are displayed.
    // This is needed because OpenGL represents images assuming the image origin is at the bottom-left
    // corner, whereas MediaPipe in general assumes the image origin is at top-left.
    private static final boolean FLIP_FRAMES_VERTICALLY = true;

    private YogaCourse yogaCourse;




    // control
    private TextView text_num;

    private float count;
    private boolean isStart = false;
    private Button bt_ctl;
    private ImageButton bt_exit;

    private Judgement judgement;

    private VideoView videoView;



    //new
    private PreviewView previewView;
    private OverlayView overlayView;
    private PoseLandmarkerHelper poseLandmarkerHelper;
    private MainViewModel viewModel;
    private Preview preview;
    private ImageAnalysis imageAnalyzer;
    private Camera camera;
    private ProcessCameraProvider cameraProvider;
    private int cameraFacing = CameraSelector.LENS_FACING_FRONT;
    private CameraSelector cameraSelector;

    /** Blocking ML operations are performed using this executor */
    private ExecutorService backgroundExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        super.onCreate(savedInstanceState);
        setContentView(getContentViewLayoutResId());
        Bundle bundle = this.getIntent().getExtras();
        String sportsName = bundle.getString("sports");
        judgement = JudgementFactory.getJudgementFactory(sportsName);
        //new
        backgroundExecutor = Executors.newSingleThreadExecutor();
        previewView = findViewById(R.id.view_finder);
        overlayView = findViewById(R.id.overlay);
        previewView.post(this::setUpCamera);

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        backgroundExecutor.execute(() -> {
            poseLandmarkerHelper = new PoseLandmarkerHelper(
                    getApplicationContext(),
                    RunningMode.LIVE_STREAM,
                    viewModel.getCurrentMinHandDetectionConfidence(),
                    viewModel.getCurrentMinHandTrackingConfidence(),
                    viewModel.getCurrentMinHandPresenceConfidence(),
                    viewModel.getCurrentDelegate(),
                    this
            );
        });

        yogaCourse = new YogaCourse();
        yogaCourse.setId(bundle.getInt(Constants.COURSE_ID));
        yogaCourse.setVideo(bundle.getString(Constants.COURSE_VIDEO));
        text_num = findViewById(R.id.text_num);
        bt_ctl = findViewById(R.id.bt_ctl);
        videoView = findViewById(R.id.video);
        bt_ctl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStart) {
                    Date date = new Date();
                    //isStart = false;
                    Intent intent = new Intent(getApplicationContext(), CountActivity.class);
                    System.out.println("PoseRecongtionActicity count:"+count);
                    intent.putExtra("count", count);
                    startActivity(intent);

                } else {
                    isStart = true;
                    bt_ctl.setText("结束");
                    Toast.makeText(getApplicationContext(), "运动开始", Toast.LENGTH_SHORT).show();
                    //播放视频
                    videoView.start();
                }
            }
        });
        bt_exit = findViewById(R.id.bt_exit);
        bt_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ContentActivity.class);
                startActivity(intent);
            }
        });


        //设置视频地址
        String filePath = yogaCourse.getVideo();
        Log.i("msg", "视频地址：" + filePath);
        if (filePath != null && filePath.length() != 0) {
            Uri uri = Uri.parse(filePath);
            videoView.setVideoURI(uri);
        } else {
            //视频地址错误
        }



        /*try {
            applicationInfo =
                    getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Cannot find application info: " + e);
        }
        // Initialize asset manager so that MediaPipe native libraries can access the app assets, e.g.,
        // binary graphs.
        AndroidAssetUtil.initializeNativeAssetManager(this);


        ///previewDisplayView = new MySurfaceView(this, processor);
        previewDisplayView = new SurfaceView(this);
        setupPreviewDisplayView();*/


    }


    // Used to obtain the content view for this application. If you are extending this class, and
    // have a custom layout, override this method and return the custom layout.
    protected int getContentViewLayoutResId() {
        return R.layout.activity_pose_recognition;
    }

    @Override
    protected void onResume() {
        super.onResume();
        backgroundExecutor.execute(() ->{
            if (poseLandmarkerHelper.isClose())  {
                poseLandmarkerHelper.setupPoseLandmarker();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (poseLandmarkerHelper != null) {
            viewModel.setMinHandDetectionConfidence(poseLandmarkerHelper.minPoseDetectionConfidence);
            viewModel.setMinHandTrackingConfidence(poseLandmarkerHelper.minPoseTrackingConfidence);
            viewModel.setMinHandTrackingConfidence(poseLandmarkerHelper.minPosePresenceConfidence);
            viewModel.setDelegate(poseLandmarkerHelper.currentDelegate);

            backgroundExecutor.execute(poseLandmarkerHelper::clearPoseLandmarker);
        }

        // Hide preview display until we re-open the camera again.
        //previewDisplayView.setVisibility(View.GONE);
    }


    /*protected void onCameraStarted(SurfaceTexture surfaceTexture) {
        previewFrameTexture = surfaceTexture;

        // Make the display view visible to start showing the preview. This triggers the
        // SurfaceHolder.Callback added to (the holder of) previewDisplayView.
        previewDisplayView.setVisibility(View.VISIBLE);
    }*/

    protected Size cameraTargetResolution() {
        return null; // No preference and let the camera (helper) decide.
    }



    protected Size computeViewSize(int width, int height) {
        return new Size(width, height);
    }



    /*private void setupPreviewDisplayView() {
        previewDisplayView.setVisibility(View.GONE);
        ViewGroup viewGroup = findViewById(R.id.preview_display_layout);
        viewGroup.addView(previewDisplayView);


    }*/

    // 提取 landmark 的坐标。
    private static String getPoseLandmarksDebugString(NormalizedLandmarkList poseLandmarks) {
        String poseLandmarkStr = "Pose landmarks: " + poseLandmarks.getLandmarkCount() + "\n";
        ArrayList<PoseLandMark> poseMarkers = new ArrayList<PoseLandMark>();
        for (NormalizedLandmark landmark : poseLandmarks.getLandmarkList()) {
            PoseLandMark marker = new PoseLandMark(landmark.getX(), landmark.getY(), landmark.getVisibility());
            //System.out.println("[" + landmark.getX() + "," + landmark.getY() + "," + landmark.getZ() + "],");
            poseMarkers.add(marker);
        }

        PoseLandMark right_hit = poseMarkers.get(24);
        PoseLandMark right_knee = poseMarkers.get(26);
        PoseLandMark right_foot = poseMarkers.get(28);
        PoseLandMark right_shoulder = poseMarkers.get(12);
        PoseLandMark right_elbow = poseMarkers.get(14);
        PoseLandMark right_hand = poseMarkers.get(16);

        double right_elbow_angle = getAngle(right_shoulder, right_elbow, right_hand);
        double right_hit_angle = getAngle(right_shoulder, right_hit, right_knee);
        double right_knee_angle = getAngle(right_hit, right_knee, right_foot);

        System.out.println("=================================");
        System.out.print("right_elbow_angle: " +right_elbow_angle + "\t");
        System.out.print("right_hit_angle: " + right_hit_angle + "\t");
        System.out.println("right_knee_angle: " + right_knee_angle);
        System.out.println("=================================");


        return poseLandmarkStr;
        /*
            16 右手腕      14 右肘   12 右肩 -> 右臂角度
            15 左手腕      13 左肘   11 左肩 -> 左臂角度
            24 右骨盆      26 右膝   28 右脚踝 -> 右膝角度
            23 左骨盆      25 左膝   27 左脚踝 -> 左膝角度
            14 右肘       12 右肩   24 右骨盆 -> 右下臂角度
            13 左肘       11 左肩   23 左骨盆 -> 左下臂角度
        */
    }

    // 根据三个点计算关节角度
    static double getAngle(PoseLandMark firstPoint, PoseLandMark midPoint, PoseLandMark lastPoint) {
        double result =
                Math.toDegrees(
                        Math.atan2(lastPoint.getY() - midPoint.getY(), lastPoint.getX() - midPoint.getX())
                                - Math.atan2(firstPoint.getY() - midPoint.getY(), firstPoint.getX() - midPoint.getX()));
        result = Math.abs(result); // Angle should never be negative
        if (result > 180) {
            result = (360.0 - result); // Always get the acute representation of the angle
        }
        return result;
    }

    static PoseLandMark getMid(PoseLandMark left_hip, PoseLandMark right_hip) {
        float x = (left_hip.getX() + right_hip.getX()) / 2;
        float y = (left_hip.getY() + right_hip.getY()) / 2;
        float visible = (left_hip.getVisible() + right_hip.getVisible()) / 2;
        PoseLandMark mid_hip = new PoseLandMark(x, y, visible);
        return mid_hip;
    }



    private void setUpCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture =
                ProcessCameraProvider.getInstance(getApplicationContext());
        cameraProviderFuture.addListener(() -> {
            // CameraProvider
            try {
                cameraProvider = cameraProviderFuture.get();
                // Build and bind the camera use cases
                bindCameraUseCases();
            } catch (Exception e) {
                Log.e(TAG, "Camera initialization failed", e);
            }
        }, ContextCompat.getMainExecutor(getApplicationContext()));
    }

    // Declare and bind preview, capture and analysis use cases
    @SuppressLint("UnsafeOptInUsageError")
    private void bindCameraUseCases() {
        // CameraProvider
        if (cameraProvider == null) {
            throw new IllegalStateException("Camera initialization failed.");
        }

        cameraSelector = new CameraSelector.Builder().requireLensFacing(cameraFacing).build();

        // Preview. Only using the 4:3 ratio because this is the closest to our models
        preview = new Preview.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                .setTargetRotation(previewView.getDisplay().getRotation())
                .build();

        // ImageAnalysis. Using RGBA 8888 to match how our models work
        imageAnalyzer = new ImageAnalysis.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                .setTargetRotation(previewView.getDisplay().getRotation())
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .setOutputImageFormat(ImageAnalysis.OUTPUT_IMAGE_FORMAT_RGBA_8888)
                .build();

        imageAnalyzer.setAnalyzer(backgroundExecutor, image -> detectPose(image));

        // Must unbind the use-cases before rebinding them
        cameraProvider.unbindAll();

        try {
            // A variable number of use-cases can be passed here -
            // camera provides access to CameraControl & CameraInfo
            camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalyzer);

            // Attach the viewfinder's surface provider to preview use case
            preview.setSurfaceProvider(previewView.getSurfaceProvider());
        } catch (Exception exc) {
            Log.e(TAG, "Use case binding failed", exc);
        }
    }

    private void detectPose(ImageProxy imageProxy) {
        if (poseLandmarkerHelper !=  null) {
            poseLandmarkerHelper.detectLiveStream(
                    imageProxy,
                    cameraFacing == CameraSelector.LENS_FACING_FRONT
            );
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (imageAnalyzer != null) {
            imageAnalyzer.setTargetRotation(previewView.getDisplay().getRotation());
        }
    }

    @Override
    public void onError(String error, int errorCode) {

    }

    @Override
    public void onResults(PoseLandmarkerHelper.ResultBundle resultBundle) {

        ArrayList<PoseLandMark> poseLandMarks = new ArrayList<>();
        for (PoseLandmarkerResult result : resultBundle.results) {
            for (List<com.google.mediapipe.tasks.components.containers.NormalizedLandmark> landmarks : result.landmarks()) {
                for (com.google.mediapipe.tasks.components.containers.NormalizedLandmark normalizedLandmark : landmarks) {
                    PoseLandMark poseLandMark = new PoseLandMark(normalizedLandmark.x(), normalizedLandmark.y(), normalizedLandmark.visibility().get());
                    poseLandMarks.add(poseLandMark);
                }
            }
        }
        judgement.init(poseLandMarks, false);
        count = judgement.countTheNum();
        runOnUiThread(() -> {
            text_num.setText(String.valueOf((int) count));
            System.out.println("获取关键点集合容量：" + poseLandMarks.size());
            System.out.println("处理结果：" + count);
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (overlayView != null) {
                    // Pass necessary information to OverlayView for drawing on the canvas
                    overlayView.setResults(
                            resultBundle.results.get(0),
                            resultBundle.inputImageHeight,
                            resultBundle.inputImageWidth,
                            RunningMode.LIVE_STREAM
                    );

                    // Force a redraw
                    overlayView.invalidate();
                }
            }
        });
    }
}