package com.example.aiyoga.activity.enity;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 训练记录列表信息
 *
 * @Author lsm
 * @CreateData 2024/6/13 11:05
 */

public class PractiseListVO {
    //@ApiModelProperty("主键id")
    private Integer id;

    //@ApiModelProperty("课程id")
    private Integer courseId;

    //@ApiModelProperty("课程名称")
    private String courseName;

    //@ApiModelProperty("用户id")
    private Integer userId;

    //@ApiModelProperty("消耗卡路里")
    private String calorie;

    //@ApiModelProperty("训练时长")
    private Long time;

    //@ApiModelProperty("训练评估得分")
    private Float score;

    //@ApiModelProperty("训练日期")
    private LocalDate trainingDate;

    public PractiseListVO() {
    }

    public PractiseListVO(Integer id, Integer courseId, String courseName, Integer userId, String calorie, Long time, Float score, LocalDate trainingDate) {
        this.id = id;
        this.courseId = courseId;
        this.courseName = courseName;
        this.userId = userId;
        this.calorie = calorie;
        this.time = time;
        this.score = score;
        this.trainingDate = trainingDate;
    }

    /**
     * 获取
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取
     * @return courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * 设置
     * @param courseId
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * 获取
     * @return courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * 设置
     * @param courseName
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * 获取
     * @return userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取
     * @return calorie
     */
    public String getCalorie() {
        return calorie;
    }

    /**
     * 设置
     * @param calorie
     */
    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    /**
     * 获取
     * @return time
     */
    public Long getTime() {
        return time;
    }

    /**
     * 设置
     * @param time
     */
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * 获取
     * @return score
     */
    public Float getScore() {
        return score;
    }

    /**
     * 设置
     * @param score
     */
    public void setScore(Float score) {
        this.score = score;
    }

    /**
     * 获取
     * @return trainingDate
     */
    public LocalDate getTrainingDate() {
        return trainingDate;
    }

    /**
     * 设置
     * @param trainingDate
     */
    public void setTrainingDate(LocalDate trainingDate) {
        this.trainingDate = trainingDate;
    }

    public String toString() {
        return "PractiseListVO{id = " + id + ", courseId = " + courseId + ", courseName = " + courseName + ", userId = " + userId + ", calorie = " + calorie + ", time = " + time + ", score = " + score + ", trainingDate = " + trainingDate + "}";
    }
}
