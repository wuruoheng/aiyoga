package com.example.aiyoga.activity.person;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.aiyoga.R;

public class SettingActivity extends AppCompatActivity {
    private LinearLayout settingLine, settingQusetionnaire, settingSafe,settingFill;
    private SettingLineOnClickListener settingLineOnClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        findViews();

        setListeners();


    }

    private void setListeners() {
        settingLineOnClickListener = new SettingLineOnClickListener();
        settingLine.setOnClickListener(settingLineOnClickListener);
        settingQusetionnaire.setOnClickListener(settingLineOnClickListener);
        settingSafe.setOnClickListener(settingLineOnClickListener);
        settingFill.setOnClickListener(settingLineOnClickListener);
    }

    private void findViews() {
        settingLine = findViewById(R.id.setting_line);
        settingQusetionnaire = findViewById(R.id.setting_questionnaire);
        settingSafe = findViewById(R.id.setting_safe);
        settingFill = findViewById(R.id.setting_fill);
    }

    class SettingLineOnClickListener implements View.OnClickListener{
        //TODO 设置跳转,去除注释
        Intent intent;
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.setting_line:
//                    intent = new Intent(SettingActivity.this, LineActivity.class);
//                    startActivity(intent);
                    break;
                case R.id.setting_questionnaire:
//                    intent = new Intent(SettingActivity.this, QusetionnaireActivity.class);
//                    startActivity(intent);
                    break;
                case R.id.setting_safe:
//                    intent = new Intent(SettingActivity.this, SafeActivity.class);
//                    startActivity(intent);
                    break;
                case R.id.setting_fill:
//                    intent = new Intent(SettingActivity.this, FillActivity.class);
//                    startActivity(intent);
                    break;
            }
        }
    }
}