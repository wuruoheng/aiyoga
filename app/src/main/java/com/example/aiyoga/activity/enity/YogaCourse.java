package com.example.aiyoga.activity.enity;

import java.util.List;

/**
 * 瑜伽课程实体
 */
public class YogaCourse {
    private Integer id;

    //@ApiModelProperty("课程名称")
    private String name;

    //@ApiModelProperty("练习时长（毫秒值）")
    private Long time;

    //@ApiModelProperty("课程等级 （1初级 2进阶 3高级")
    private Integer grade;

    //@ApiModelProperty("课程图片")
    private String image;

    ///@ApiModelProperty("课程视频")
    private String video;

    //@ApiModelProperty("消耗卡路里")
    private String calorie;

    //@ApiModelProperty("练习次数")
    private Integer count;

    //@ApiModelProperty("用户状态 （收藏1 练习过2 收藏并练习过3）")
    private Integer state;

    //@ApiModelProperty("课程介绍")
    private String introduction;

    //@ApiModelProperty("练习建议")
    private String advice;

    //@ApiModelProperty("课程评论")
    private List<YogaCourseComment> comments;

    public YogaCourse() {
    }

    public YogaCourse(Integer id, String name, Long time, Integer grade, String image, String video, String calorie, Integer count, Integer state, String introduction, String advice, List<YogaCourseComment> comments) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.grade = grade;
        this.image = image;
        this.video = video;
        this.calorie = calorie;
        this.count = count;
        this.state = state;
        this.introduction = introduction;
        this.advice = advice;
        this.comments = comments;
    }

    /**
     * 获取
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return time
     */
    public Long getTime() {
        return time;
    }

    /**
     * 设置
     * @param time
     */
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * 获取
     * @return grade
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 设置
     * @param grade
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * 获取
     * @return image
     */
    public String getImage() {
        return image;
    }

    /**
     * 设置
     * @param image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 获取
     * @return video
     */
    public String getVideo() {
        return video;
    }

    /**
     * 设置
     * @param video
     */
    public void setVideo(String video) {
        this.video = video;
    }

    /**
     * 获取
     * @return calorie
     */
    public String getCalorie() {
        return calorie;
    }

    /**
     * 设置
     * @param calorie
     */
    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    /**
     * 获取
     * @return count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 设置
     * @param count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 获取
     * @return state
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置
     * @param state
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取
     * @return introduction
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置
     * @param introduction
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取
     * @return advice
     */
    public String getAdvice() {
        return advice;
    }

    /**
     * 设置
     * @param advice
     */
    public void setAdvice(String advice) {
        this.advice = advice;
    }

    /**
     * 获取
     * @return comments
     */
    public List<YogaCourseComment> getComments() {
        return comments;
    }

    /**
     * 设置
     * @param comments
     */
    public void setComments(List<YogaCourseComment> comments) {
        this.comments = comments;
    }

    public String toString() {
        return "YogaCourse{id = " + id + ", name = " + name + ", time = " + time + ", grade = " + grade + ", image = " + image + ", video = " + video + ", calorie = " + calorie + ", count = " + count + ", state = " + state + ", introduction = " + introduction + ", advice = " + advice + ", comments = " + comments + "}";
    }
}
