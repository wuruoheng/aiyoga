package com.example.aiyoga.activity.enity;

public class ClassAction {
    /**
     *
     * 			"grade": 0,
     * 			"id": 0,
     * 			"image": "",
     * 			"name": "",
     * 			"type": ""*/
    private int grade;
    private int id;
    private String image;
    private String name;
    private String type;

    public ClassAction() {
    }

    public ClassAction(int grade, int id, String image, String name, String type) {
        this.grade = grade;
        this.id = id;
        this.image = image;
        this.name = name;
        this.type = type;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
