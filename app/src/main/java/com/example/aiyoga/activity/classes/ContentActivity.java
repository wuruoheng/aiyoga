package com.example.aiyoga.activity.classes;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.PractiseListVO;
import com.example.aiyoga.activity.enity.Result;
import com.example.aiyoga.activity.enity.YogaCourse;
import com.example.aiyoga.utils.ApiUrl;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.OkHttpUtils;
import com.example.aiyoga.utils.PreferenceManager;
import com.example.aiyoga.utils.gson.GsonManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ContentActivity extends AppCompatActivity {
    private Button btnRevise,btnPoseExercise;
    private  MyOnClickListener myOnClickListener;
    private YogaCourse yogaCourse;
    private PreferenceManager preferenceManager;
    private TextView time;
    private TextView grade;
    private TextView calorie;
    private TextView count;
    private TextView advice;
    private VideoView videoView;
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        preferenceManager = new PreferenceManager(getApplicationContext());
        gson = GsonManager.get();
        findViews();
        initdata();

        setListener();
    }

    private void initdata() {
        //获取课程id
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");

        //根据id查询课程详细信息
        new Thread(new Runnable() {
            @Override
            public void run() {
                String resultJson = OkHttpUtils.doGet(ApiUrl.GET_YOGA_COURSE_MSG + id, preferenceManager.getString(Constants.USER_TOKEN));
                Result<YogaCourse> result = gson.fromJson(resultJson, new TypeToken<Result<YogaCourse>>(){}.getType());
                Log.i("msg", result.toString());
                if (result.getCode() == 1) {
                    //获取瑜伽课程数据
                    yogaCourse = result.getData();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //设置课程信息
                            setCourseMsg(yogaCourse);
                        }
                    });
                }
            }
        }).start();

    }

    private void setCourseMsg(YogaCourse yogaCourse) {
        long minute = yogaCourse.getTime() / 1000 / 60;
        long sencond = yogaCourse.getTime() / 1000 % 60;
        StringBuilder courseTime = new StringBuilder(minute + ":");
        if (minute < 10) {
            courseTime.insert(0, "0");
        }
        if (sencond < 10) {
            courseTime.append("0");
        }
        courseTime.append(sencond);
        time.setText(courseTime.toString());
        grade.setText(Constants.COURSE_GRADE[yogaCourse.getGrade()]);
        calorie.setText(yogaCourse.getCalorie());
        count.setText(yogaCourse.getCount() + "人学习");
        String[] advices = gson.fromJson(yogaCourse.getAdvice(), String[].class);
        StringBuilder msg = new StringBuilder();
        for (String item : advices) {
            msg.append(item + "\n");
        }
        advice.setText(msg.toString());
        videoView.setVideoURI(Uri.parse(yogaCourse.getVideo()));
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // 视频播放完后的操作
            }
        });
        // 添加视频控制器
        MediaController mediaController = new MediaController(this);

        videoView.setMediaController(mediaController);
        // 监视视频播放准备事件
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // 视频准备完成后开始播放
                // 获取视频的总时长
               /* int duration = videoView.getDuration();
                // 根据用户的操作，设置要跳转的播放进度
                int targetProgress = 0; // 要跳转的播放进度，单位为毫秒

                // 设置播放进度
                videoView.seekTo(targetProgress);*/
                videoView.start();
            }
        });

    }




    private void setListener() {
        myOnClickListener = new MyOnClickListener();
        btnRevise.setOnClickListener(myOnClickListener);
        btnPoseExercise.setOnClickListener(myOnClickListener);
    }

    private void findViews() {
        videoView = findViewById(R.id.vd_film);
        btnRevise = findViewById(R.id.btn_revise_exercise);
        btnPoseExercise = findViewById(R.id.btn_pose_exercise);
        time = findViewById(R.id.tv_time);
        grade = findViewById(R.id.tv_grade);
        calorie = findViewById(R.id.tv_count);
        count = findViewById(R.id.tv_count);
        advice = findViewById(R.id.tv_advice);
    }

    class MyOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn_revise_exercise:
                    Intent intent = new Intent(ContentActivity.this, ReviseActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btn_pose_exercise:
                    Bundle bundle = new Bundle();
                    bundle.putString("sports", "yoga");
                    bundle.putInt(Constants.COURSE_ID, yogaCourse.getId());
                    bundle.putString(Constants.COURSE_VIDEO, yogaCourse.getVideo());
                    Intent intent1 = new Intent(ContentActivity.this, PoseRecognitionActivity.class);
                    intent1.putExtras(bundle);
                    startActivity(intent1);
                    break;
                case R.id.vd_film:
                    break;
            }
        }
    }
}