package com.example.aiyoga.activity.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aiyoga.R;
import com.example.aiyoga.activity.classes.ClassItemActivity;
import com.example.aiyoga.activity.enity.Class;
import com.example.aiyoga.adapter.GrideAdapter;
import com.youth.banner.Banner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class FirstFragment extends Fragment {


    private Button btnPerson,  btnCommunity ,btnExercise;
    private LinearLayout layout;

    ImageView imageFirst,imgAvatar;

    private TextView tv_title;



    private GridView gridView;
    ArrayList<Integer> banner_data;

    private Banner banner;

    List<Class> goods = new ArrayList<>();

    public FirstFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FirstFragment newInstance(String param1, String param2) {
        FirstFragment fragment = new FirstFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
//        gridView = view.findViewById(R.id.gr_goods);
//        banner = view.findViewById(R.id.first_banner);

        //banner的图片源
        //initData();

        //初始化数据源
//        initDate();
//        //实例化ArrayAdapter的配适器对象
//        GrideAdapter goodAdapter = new GrideAdapter(
//                getContext(),
//                R.layout.fragment_first_item,
//                goods
//        );
//        banner.setAdapter(new BannerImageAdapter<Integer>(banner_data) {
//
//            @Override
//            public void onBindView(BannerImageHolder holder, Integer data, int position, int size) {
//                holder.imageView.setImageResource(data);
//            }
//        });
//
//        gridView.setAdapter(goodAdapter);
//        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                //跳转到详情页
//                Intent intent = new Intent(getContext(), ClassItemActivity.class);
//                intent.putExtra("class",goods.get(position).getClassName().toString());
//                getContext().startActivity(intent);
//            }
//        });
//        layout.bringToFront();
        Bundle bundle =this.getArguments();//得到从Activity传来的数据
        String userName = null;
        String userAvatar = null;
        String userGender = null;
        String userAccount = null;
        if(bundle!=null){
            userName = bundle.getString("userName");
            userAvatar = bundle.getString("userAvatar");
            userGender = bundle.getString("userGender");
            userAccount = bundle.getString("userAccount");
        }


        imageFirst = view.findViewById(R.id.img_first);
        tv_title = view.findViewById(R.id.tx_user);
        imgAvatar = view.findViewById(R.id.img_three_first);

        //获取当前的时间判断是上午，下午还是中午
        GregorianCalendar ca = new GregorianCalendar();
        ca.setTime(new Date());
        String s = (ca.get(GregorianCalendar.AM_PM)== Calendar.PM ? "下午好！" : "上午好！");
        tv_title.setText(s+userName);

        //头像,圆形显示
        Glide.with(this)
                .load(userAvatar)
                .circleCrop()
                .into(imgAvatar);


        CenListener cenListener = new CenListener();
        imageFirst.setOnClickListener(cenListener);

        return view;
    }



    private void initData(){
        banner_data = new ArrayList<>();
        banner_data.add(R.drawable.banner001);
        banner_data.add(R.drawable.banner002);
        banner_data.add(R.drawable.banner003);
        banner_data.add(R.drawable.banner004);
        banner_data.add(R.drawable.banner005);
    }

    private void initDate() {
        goods = new ArrayList<>();
        Class class1 = new Class("1","初级课程");
        goods.add(class1);
        Class class2 = new Class("2","进阶课程");
        goods.add(class2);
        Class class3 = new Class("3","高级课程");
        goods.add(class3);
        Class class4 = new Class("4","动作库");
        goods.add(class4);
    }

    class CenListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_first:
                    Intent intent = new Intent(getContext(), ClassItemActivity.class);
                    intent.putExtra("class","初级课程");
                    getContext().startActivity(intent);
            }
        }
    }


}