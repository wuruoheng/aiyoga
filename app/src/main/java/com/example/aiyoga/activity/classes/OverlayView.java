package com.example.aiyoga.activity.classes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.example.aiyoga.R;
import com.google.mediapipe.tasks.components.containers.NormalizedLandmark;
import com.google.mediapipe.tasks.vision.core.RunningMode;
import com.google.mediapipe.tasks.vision.poselandmarker.PoseLandmarker;
import com.google.mediapipe.tasks.vision.poselandmarker.PoseLandmarkerResult;

import java.util.List;

public class OverlayView extends View {
    private PoseLandmarkerResult results = null;
    private final Paint linePaint;
    private final Paint pointPaint;

    private float scaleFactor = 1f;
    private int imageWidth = 1;
    private int imageHeight = 1;

    private static final float LANDMARK_STROKE_WIDTH = 12F;

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        linePaint = new Paint();
        pointPaint = new Paint();
        initPaints(context);
    }

    public void clear() {
        results = null;
        linePaint.reset();
        pointPaint.reset();
        invalidate();
        initPaints(getContext());
    }

    private void initPaints(Context context) {
        linePaint.setColor(ContextCompat.getColor(context, R.color.mp_color_primary));
        linePaint.setStrokeWidth(LANDMARK_STROKE_WIDTH);
        linePaint.setStyle(Paint.Style.STROKE);

        pointPaint.setColor(Color.YELLOW);
        pointPaint.setStrokeWidth(LANDMARK_STROKE_WIDTH);
        pointPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (results != null) {
            for (List<NormalizedLandmark> landmark : results.landmarks()) {
                for (NormalizedLandmark normalizedLandmark : landmark) {
                    canvas.drawPoint(
                            normalizedLandmark.x() * imageWidth * scaleFactor,
                            normalizedLandmark.y() * imageHeight * scaleFactor,
                            pointPaint
                    );
                }

                PoseLandmarker.POSE_LANDMARKS.forEach(connection -> {
                    canvas.drawLine(
                            results.landmarks().get(0).get(connection.start()).x() * imageWidth * scaleFactor,
                            results.landmarks().get(0).get(connection.start()).y() * imageHeight * scaleFactor,
                            results.landmarks().get(0).get(connection.end()).x() * imageWidth * scaleFactor,
                            results.landmarks().get(0).get(connection.end()).y() * imageHeight * scaleFactor,
                            linePaint
                    );
                });
            }

        }
    }

    public void setResults(PoseLandmarkerResult poseLandmarkerResults, int imageHeight, int imageWidth, RunningMode runningMode) {
        if (runningMode == null) runningMode = RunningMode.IMAGE;
        results = poseLandmarkerResults;
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;

        switch (runningMode) {
            case IMAGE:
            case VIDEO:
                scaleFactor = Math.min((float) getWidth() / imageWidth, (float) getHeight() / imageHeight);
                break;
            case LIVE_STREAM:
                scaleFactor = Math.max((float) getWidth() / imageWidth, (float) getHeight() / imageHeight);
                break;
        }

        invalidate();
    }


}
