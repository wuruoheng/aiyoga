package com.example.aiyoga.activity.enity;

public class ItemCommunity {
    //用户昵称
    private String username;
    //帖子内容
    private String content;
    //帖子图片
    private String imgUrl;
    //帖子发布时间
    private String time;
//    用户头像
    private String userImg;

    public ItemCommunity() {
    }

    public ItemCommunity(String username, String content, String imgUrl, String time, String userImg) {
        this.username = username;
        this.content = content;
        this.imgUrl = imgUrl;
        this.time = time;
        this.userImg = userImg;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    @Override
    public String toString() {
        return "ItemCommunity{" +
                "username='" + username + '\'' +
                ", content='" + content + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", time='" + time + '\'' +
                ", userImg='" + userImg + '\'' +
                '}';
    }
}
