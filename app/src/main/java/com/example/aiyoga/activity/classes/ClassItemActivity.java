package com.example.aiyoga.activity.classes;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.aiyoga.R;
import com.example.aiyoga.activity.enity.Class;
import com.example.aiyoga.activity.enity.ClassAction;
import com.example.aiyoga.activity.enity.ClassDetail;
import com.example.aiyoga.activity.enity.Result;
import com.example.aiyoga.adapter.ClassActionAdapter;
import com.example.aiyoga.adapter.ClassAdapter;
import com.example.aiyoga.utils.ApiUrl;
import com.example.aiyoga.utils.Constants;
import com.example.aiyoga.utils.OkHttpUtils;
import com.example.aiyoga.utils.PreferenceManager;
import com.example.aiyoga.utils.gson.GsonManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class ClassItemActivity extends AppCompatActivity {

    private GridView gridView;
    private PreferenceManager preferenceManager;
    private SearchView searchView;
    private String[] mStrs = {"aaa", "bbb", "ccc", "airsaid"};

    private ListView mListView;

    List<ClassAction> classActions = new ArrayList<>();
    List<ClassDetail> classDetails = new ArrayList<>();
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_item);

        gridView = findViewById(R.id.class_item);
        searchView = findViewById(R.id.searchView);
        mListView = findViewById(R.id.listView);
        gson = GsonManager.get();
        preferenceManager = new PreferenceManager(getApplicationContext());

        Intent intent = getIntent();
        String aClass = intent.getStringExtra("class");

        //弹窗显示aClass内容
        Toast.makeText(this,aClass,Toast.LENGTH_SHORT).show();

        //初始化数据源
        //实例化ArrayAdapter的配适器对象
        if (aClass.equals("动作库")){
            initActionData();
            mListView.setVisibility(View.GONE);
            mListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mStrs));
            mListView.setTextFilterEnabled(true);

            // 设置搜索文本监听
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                // 当点击搜索按钮时触发该方法
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                // 当搜索内容改变时触发该方法
                @Override
                public boolean onQueryTextChange(String newText) {
                    gridView.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(newText)){
                        mListView.setVisibility(View.VISIBLE);
                        mListView.setFilterText(newText);
                    }else{
                        mListView.clearTextFilter();
                        gridView.setVisibility(View.VISIBLE);
                        mListView.setVisibility(View.GONE);
                    }
                    return false;
                }
            });


        } else if (aClass.equals("初级课程")){
            searchView.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
           initClassData(1);
        } else if (aClass.equals("高级课程")){
            searchView.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
            initClassData(3);
        } else {
            searchView.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
            initClassData(2);
        }




    }


    //获取课程数据
    private void initClassData(int grade){
        new Thread(new Runnable() {

            @Override
            public void run() {

                String jsonResult = OkHttpUtils.doGet(ApiUrl.GET_ClASS_LIST+"?grade="+grade, preferenceManager.getString(Constants.USER_TOKEN));
                Result<List<ClassDetail>> result = gson.fromJson(jsonResult, new TypeToken<Result<List<ClassDetail>>>(){}.getType());
                if (result.getCode() == 1) {
                    classDetails = result.getData();
                    Log.i("i", classDetails.get(0).getName());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ClassAdapter adapter = new ClassAdapter(
                                    ClassItemActivity.this,
                                    R.layout.activity_class_detail,
                                    classDetails,
                                    preferenceManager
                            );

                            gridView.setAdapter(adapter);
                            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    //跳转到详情页
                                    Intent intent = new Intent(getApplicationContext(), ContentActivity.class);
                                    intent.putExtra("id",classDetails.get(position).getId()+"");
                                    startActivity(intent);
                                }
                            });
                        }
                    });
                }
            }
        }).start();
    }

    private void initActionData() {
        //获取动作库列表
        new Thread(new Runnable() {
            @Override
            public void run() {
                String jsonResult = OkHttpUtils.doGet(ApiUrl.GET_ACTION_LIST, preferenceManager.getString(Constants.USER_TOKEN));
                Log.i("i", jsonResult);
                Result<List<ClassAction>> result = gson.fromJson(jsonResult, new TypeToken<Result<List<ClassAction>>>(){}.getType());
                if (result.getCode() == 1) {
                    classActions = result.getData();
                    Log.i("i", classActions.toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ClassActionAdapter adapter = new ClassActionAdapter(
                                    ClassItemActivity.this,
                                    R.layout.activity_class_action,
                                    classActions,
                                    preferenceManager
                            );
                            gridView.setAdapter(adapter);
                            for (ClassAction classDetail : classActions) {
                              System.out.println("classDetailName"+classDetail.getName());
                            }


                        }
                    });
                }
            }
        }).start();



    }
}