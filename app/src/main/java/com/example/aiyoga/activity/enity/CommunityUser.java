package com.example.aiyoga.activity.enity;

public class CommunityUser {
    private String username;
    private String avatar;
    public CommunityUser(String username, String avatar) {
        this.username = username;
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    @Override
    public String toString() {
        return "CommunityUser [username=" + username + ", avatar=" + avatar + "]";
    }
}
