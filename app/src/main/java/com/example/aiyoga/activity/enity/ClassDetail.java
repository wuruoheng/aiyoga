package com.example.aiyoga.activity.enity;

public class ClassDetail {
    //   	"calorie": "",
//            "count": 0,
//            "grade": 0,
//            "id": 0,
//            "image": "",
//            "name": "",
//            "time":
    private String calorie;
    private int count;
    private int grade;
    private int id;
    private String image;
    private String name;
    private int time;

    public ClassDetail() {
    }

    public ClassDetail(String calorie, int count, int grade, int id, String image, String name,int time) {
        this.calorie = calorie;
        this.count = count;
        this.grade = grade;
        this.id = id;
        this.image = image;
        this.name = name;
        this.time = time;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
