package com.example.aiyoga.activity.enity;

/**
 * 用户登录后保存的信息
 */
public class UserLoginMsg {
    //昵称
    private String name;

    //账号
    private String account;

    //性别
    private String gender;

    //头像
    private String avatar;

    private String token;

    public UserLoginMsg() {
    }

    public UserLoginMsg(String name, String account, String gender, String avatar, String token) {
        this.name = name;
        this.account = account;
        this.gender = gender;
        this.avatar = avatar;
        this.token = token;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return account
     */
    public String getAccount() {
        return account;
    }

    /**
     * 设置
     * @param account
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 获取
     * @return gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * 设置
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 获取
     * @return avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 设置
     * @param avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 获取
     * @return token
     */
    public String getToken() {
        return token;
    }

    /**
     * 设置
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    public String toString() {
        return "UserLoginMsg{name = " + name + ", account = " + account + ", gender = " + gender + ", avatar = " + avatar + ", token = " + token + "}";
    }
}
