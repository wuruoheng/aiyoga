package com.example.aiyoga.activity.classes;

import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {
    private int model = PoseLandmarkerHelper.MODEL_POSE_LANDMARKER_FULL;
    private int delegate = PoseLandmarkerHelper.DELEGATE_CPU;
    private float minHandDetectionConfidence = PoseLandmarkerHelper.DEFAULT_POSE_DETECTION_CONFIDENCE;
    private float minHandTrackingConfidence = PoseLandmarkerHelper.DEFAULT_POSE_TRACKING_CONFIDENCE;
    private float minHandPresenceConfidence = PoseLandmarkerHelper.DEFAULT_POSE_PRESENCE_CONFIDENCE;


    public int getCurrentDelegate() {
        return delegate;
    }
    public int getcurrentModel(){
        return model;
    }

    public float getCurrentMinHandDetectionConfidence() {
        return minHandDetectionConfidence;
    }

    public float getCurrentMinHandTrackingConfidence() {
        return minHandTrackingConfidence;
    }

    public float getCurrentMinHandPresenceConfidence() {
        return minHandPresenceConfidence;
    }


    public void setDelegate(int delegate) {
        this.delegate = delegate;
    }

    public void setMinHandDetectionConfidence(float confidence) {
        this.minHandDetectionConfidence = confidence;
    }

    public void setMinHandTrackingConfidence(float confidence) {
        this.minHandTrackingConfidence = confidence;
    }

    public void setMinHandPresenceConfidence(float confidence) {
        this.minHandPresenceConfidence = confidence;
    }

    public void setModel(int model) {
        this.model = model;
    }
}
