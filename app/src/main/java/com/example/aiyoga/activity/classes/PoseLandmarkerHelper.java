package com.example.aiyoga.activity.classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.SystemClock;
import android.util.Log;

import androidx.camera.core.ImageProxy;

import com.google.common.annotations.VisibleForTesting;
import com.google.mediapipe.framework.image.BitmapImageBuilder;
import com.google.mediapipe.framework.image.MPImage;
import com.google.mediapipe.tasks.core.BaseOptions;
import com.google.mediapipe.tasks.core.Delegate;
import com.google.mediapipe.tasks.vision.core.RunningMode;
import com.google.mediapipe.tasks.vision.poselandmarker.PoseLandmarker;
import com.google.mediapipe.tasks.vision.poselandmarker.PoseLandmarkerResult;

import java.util.List;

public class PoseLandmarkerHelper {
    public static final float DEFAULT_POSE_DETECTION_CONFIDENCE = 0.5f; // Example default
    public static final float DEFAULT_POSE_TRACKING_CONFIDENCE = 0.5f; // Example default
    public static final float DEFAULT_POSE_PRESENCE_CONFIDENCE = 0.5f; // Example default
    public static final int DELEGATE_CPU = 0;
    public static final int DELEGATE_GPU = 1;
    public  static final String TAG = "PoseLandmarkerHelper";
    public  static final int GPU_ERROR = 1; // Example error code

    public  static final int OTHER_ERROR = 0;
    public static final int MODEL_POSE_LANDMARKER_FULL = 0;
    public static final int MODEL_POSE_LANDMARKER_LITE = 1;
    public static final int MODEL_POSE_LANDMARKER_HEAVY = 2;


    public float minPoseDetectionConfidence = DEFAULT_POSE_DETECTION_CONFIDENCE;
    public float minPoseTrackingConfidence = DEFAULT_POSE_TRACKING_CONFIDENCE;
    public float minPosePresenceConfidence = DEFAULT_POSE_PRESENCE_CONFIDENCE;
    public int currentModel =  MODEL_POSE_LANDMARKER_FULL;
    public int maxNumHands;
    public int currentDelegate  = DELEGATE_CPU;
    RunningMode runningMode = RunningMode.IMAGE;
    final Context context;
    LandmarkerListener poseLandmarkerHelperListener;
    private PoseLandmarker poseLandmarker;

    public PoseLandmarkerHelper(Context applicationContext, RunningMode liveStream, float currentMinPoseDetectionConfidence, float currentMinPoseTrackingConfidence, float currentMinPosePresenceConfidence, int currentModel, int currentDelegate, PoseRecognitionActivity mainActivity) {
        this.minPoseDetectionConfidence = currentMinPoseDetectionConfidence;
        this.minPoseTrackingConfidence = currentMinPoseTrackingConfidence;
        this.minPosePresenceConfidence = currentMinPosePresenceConfidence;
        this.currentModel = currentModel;
        this.currentDelegate = currentDelegate;
        this.runningMode = liveStream;
        this.context = applicationContext;
        this.poseLandmarkerHelperListener = mainActivity;
        setupPoseLandmarker();
    }
    public PoseLandmarkerHelper(Context applicationContext, RunningMode liveStream, float currentMinPoseDetectionConfidence, float currentMinPoseTrackingConfidence, float currentMinPosePresenceConfidence, int currentDelegate, PoseRecognitionActivity mainActivity) {
        this.minPoseDetectionConfidence = currentMinPoseDetectionConfidence;
        this.minPoseTrackingConfidence = currentMinPoseTrackingConfidence;
        this.minPosePresenceConfidence = currentMinPosePresenceConfidence;
        this.currentDelegate = currentDelegate;
        this.runningMode = liveStream;
        this.context = applicationContext;
        this.poseLandmarkerHelperListener = mainActivity;
        setupPoseLandmarker();
    }

    public void clearPoseLandmarker() {
        if (poseLandmarker != null) {
            poseLandmarker.close();
            poseLandmarker = null;
        }
    }

    public boolean isClose() {
        return poseLandmarker == null;
    }

    public void setupPoseLandmarker() {
        BaseOptions.Builder baseOptionBuilder = BaseOptions.builder();

        switch (currentDelegate) {
            case DELEGATE_CPU:
                baseOptionBuilder.setDelegate(Delegate.CPU);
                break;
            case DELEGATE_GPU:
                baseOptionBuilder.setDelegate(Delegate.GPU);
                break;
        }
        String modelName;
        switch (currentModel) {
            case MODEL_POSE_LANDMARKER_FULL:
                modelName =  "pose_landmarker_full.task";
                break;
            case MODEL_POSE_LANDMARKER_LITE:
                modelName = "pose_landmarker_lite.task";
                break;
            case MODEL_POSE_LANDMARKER_HEAVY:
                modelName  =   "pose_landmarker_heavy.task";
                break;
            default:
                modelName = "pose_landmarker_full.task";
        }
        baseOptionBuilder.setModelAssetPath(modelName);

        switch (runningMode) {
            case LIVE_STREAM:
                if (poseLandmarkerHelperListener == null) {
                    throw new IllegalStateException("poseLandmarkerHelperListener must be set when runningMode is LIVE_STREAM.");
                }
                break;
            default:
                //no-op
        }

        try {
            BaseOptions baseOptions = baseOptionBuilder.build();
            PoseLandmarker.PoseLandmarkerOptions.Builder optionsBuilder = PoseLandmarker.PoseLandmarkerOptions.builder()
                    .setBaseOptions(baseOptions)
                    .setMinPoseDetectionConfidence(minPoseDetectionConfidence)
                    .setMinTrackingConfidence(minPoseTrackingConfidence)
                    .setMinPosePresenceConfidence(minPosePresenceConfidence)
                    .setRunningMode(runningMode);

            if (runningMode == RunningMode.LIVE_STREAM) {
                optionsBuilder
                        .setResultListener(this::returnLivestreamResult)
                        .setErrorListener(this::returnLivestreamError);
            }

            PoseLandmarker.PoseLandmarkerOptions options = optionsBuilder.build();
            poseLandmarker = PoseLandmarker.createFromOptions(context, options);
        } catch (IllegalStateException e) {
            if (poseLandmarkerHelperListener != null) {
                poseLandmarkerHelperListener.onError("Pose Landmarker failed to initialize. See error logs for " +
                        "details", OTHER_ERROR);
            }
            Log.e(TAG, "MediaPipe failed to load the task with error: " + e.getMessage());
        } catch (RuntimeException e) {
            if (poseLandmarkerHelperListener != null) {
                poseLandmarkerHelperListener.onError("Pose Landmarker failed to initialize. See error logs for " +
                        "details", GPU_ERROR);
            }
            Log.e(TAG, "Image classifier failed to load model with error: " + e.getMessage());
        }
    }

    public void detectLiveStream(ImageProxy imageProxy, boolean isFrontCamera) {
        if (runningMode != RunningMode.LIVE_STREAM) {
            throw new IllegalArgumentException("Attempting to call detectLiveStream while not using RunningMode.LIVE_STREAM");
        }

        long frameTime = SystemClock.uptimeMillis();

        Bitmap bitmapBuffer = Bitmap.createBitmap(imageProxy.getWidth(), imageProxy.getHeight(), Bitmap.Config.ARGB_8888);
        bitmapBuffer.copyPixelsFromBuffer(imageProxy.getPlanes()[0].getBuffer());
        //imageProxy.use(() -> bitmapBuffer.copyPixelsFromBuffer(imageProxy.getPlanes()[0].getBuffer()));
        imageProxy.close();

        Matrix matrix = new Matrix();
        matrix.postRotate((float) imageProxy.getImageInfo().getRotationDegrees());

        if (isFrontCamera) {
            matrix.postScale(-1f, 1f, (float) imageProxy.getWidth(), (float) imageProxy.getHeight());
        }

        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmapBuffer, 0, 0, bitmapBuffer.getWidth(), bitmapBuffer.getHeight(), matrix, true);
        MPImage mpImage = new BitmapImageBuilder(rotatedBitmap).build();

        detectAsync(mpImage, frameTime);
    }

    @VisibleForTesting
    public void detectAsync(MPImage mpImage, long frameTime) {
        if (poseLandmarker != null) {
            poseLandmarker.detectAsync(mpImage, frameTime);
        }
    }

    private void returnLivestreamResult(PoseLandmarkerResult result, MPImage input) {
        long finishTimeMs = SystemClock.uptimeMillis();
        long inferenceTime = finishTimeMs - result.timestampMs();
        if (poseLandmarkerHelperListener != null) {
            poseLandmarkerHelperListener.onResults(new ResultBundle(List.of(result), inferenceTime, input.getHeight(), input.getWidth()));
        }

    }

    private void returnLivestreamError(RuntimeException error) {
        //handLandmarkerHelperListener.onError(error.getMessage() != null ? error.getMessage() : "An unknown error has occurred");
    }

    public static class ResultBundle {
        public List<PoseLandmarkerResult> results;
        public long inferenceTime;
        public int inputImageHeight;
        public int inputImageWidth;

        public ResultBundle(List<PoseLandmarkerResult> results, long inferenceTime, int inputImageHeight, int inputImageWidth) {
            this.results = results;
            this.inferenceTime = inferenceTime;
            this.inputImageHeight = inputImageHeight;
            this.inputImageWidth = inputImageWidth;
        }

    }

    public interface LandmarkerListener {
        void onError(String error, int errorCode);
        void onResults(ResultBundle resultBundle);
    }
}
