package com.example.aiyoga.utils;

public class Constants {
    public final static String USER_NAME = "userName"; //用户昵称
    public final static String USER_ACCOUNT = "userAccount"; //用户账号
    public final static String USER_GENDER = "userGender"; //用户性别
    public final static String USER_AVATAR = "userAvatar"; //用户头像
    public final static String USER_TOKEN = "userToken"; //token验证

    public final static String COURSE_VIDEO = "courseVideo";//课程视频地址
    public static final String COURSE_ID = "courseId";//课程id
    public static final String[] COURSE_GRADE = {"", "初级", "进阶", "高级"};
}
