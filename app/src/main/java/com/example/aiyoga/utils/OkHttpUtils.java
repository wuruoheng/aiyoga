package com.example.aiyoga.utils;

import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpUtils {
    private static OkHttpClient okHttpClient;
    private final static String ROOT = "http://ares.nat100.top";
    private final static String TARGET = "OkHttPUtils";
    static {
        okHttpClient = new OkHttpClient();
    }

    private OkHttpUtils() {

    }

    /**
     * POST请求，以json字符串传入参数，返回json字符串
     * @param url
     * @param json
     * @return
     */
    public static String doPost(String url, String json, String token) {
        Log.i(TARGET, "POST请求");
        Log.i(TARGET, "请求url：" + ROOT +  url);
        Log.i(TARGET, "请求参数：" + json);
        RequestBody requestBody = RequestBody.create(
                MediaType.parse("application/json;charset=utf-8"),//数据类型
                json
        );
        //获取request对象
        Request request = new Request.Builder()
                .url(ROOT + url)
                .addHeader("token", token)
                .post(requestBody)
                .build();
        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * POST请求，传入图片路径,返回json字符串
     * @param url
     * @param path
     * @return
     */
    public static String upImg(String url, String path, String token) {
        Log.i(TARGET, "上传图片");
        Log.i(TARGET, "请求url：" + url);
        Log.i(TARGET, "图片地址：" + path);
        String[] split = path.split("\\.");
        String form = split[split.length - 1];
        MultipartBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                        "file",
                        "头像." + form,
                        RequestBody.create(MediaType.parse("image/**"), new File(path))
                ).build();
        //创建Request对象
        Request request = new Request.Builder()
                .addHeader("token", token)
                .post(body)
                .url(ROOT + url)
                .build();
        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String upImgs(String url, List<String> pathes, String token) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        for (int i = 0; i < pathes.size(); i++) {
            String[] split = pathes.get(i).split("\\.");
            String form = split[split.length - 1];
            builder.addFormDataPart(
                    "files",
                    "img" + i + "." + form,
                    RequestBody.create(MediaType.parse("image/**"), new File(pathes.get(i)))
            );
        }
        MultipartBody body = builder.build();
        //创建Request对象
        Request request = new Request.Builder()
                .addHeader("token", token)
                .post(body)
                .url(ROOT + url)
                .build();
        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * GET请求，参数以Map<String,String>形式传入
     * @param url
     * @return
     */
    public static String doGet(String url, String token) {
        Log.i(TARGET, "GET请求");
        Log.i(TARGET, "请求url：" + ROOT +  url);
        StringBuffer reqUrl = new StringBuffer(ROOT + url);

        Log.i(TARGET, "实际请求路径: " + reqUrl);
        Request request = new Request.Builder()
                .url(reqUrl.toString())
                .addHeader("token", token)
                .build();
        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String doDelete(String url, String token) {
        Log.i(TARGET, "DELETE请求");
        Log.i(TARGET, "请求url：" + ROOT +  url);
        StringBuffer reqUrl = new StringBuffer(ROOT + url);

        Log.i(TARGET, "实际请求路径: " + reqUrl);
        Request request = new Request.Builder()
                .delete()
                .url(reqUrl.toString())
                .addHeader("token", token)
                .build();
        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String doPut(String url, String json, String token) {
        Log.i(TARGET, "PUT请求");
        Log.i(TARGET, "请求url：" + ROOT +  url);
        Log.i(TARGET, "请求参数：" + json);
        RequestBody requestBody = RequestBody.create(
                MediaType.parse("application/json;charset=utf-8"),//数据类型
                json
        );
        //获取request对象
        Request request = new Request.Builder()
                .url(ROOT + url)
                .addHeader("token", token)
                .put(requestBody)
                .build();
        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



}
