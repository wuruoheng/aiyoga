package com.example.aiyoga.utils;

public class ApiUrl {
    public final static String GET_PRACTISE_LIST = "/practise/list";
    public final static String GET_ACTION_LIST = "/action/list";
    public final static String GET_ClASS_LIST = "/yogacourse/list";
    public final static String GET_YOGA_COURSE_MSG = "/yogacourse/courseMsg/";
}
