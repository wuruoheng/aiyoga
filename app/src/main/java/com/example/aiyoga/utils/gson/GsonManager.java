package com.example.aiyoga.utils.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class GsonManager {
    private static Gson gson;

    public static Gson get() {
        if (gson != null) {
            return gson;
        }
        gson = new GsonBuilder()
                .registerTypeAdapter(new TypeToken<LocalDate>(){}.getType(), new LocalDateConverter())
                .registerTypeAdapter(new TypeToken<LocalDateTime>(){}.getType(), new LocalDateTimeConverter())
                .setDateFormat("yyyy-MM-dd")
                .create();
        return gson;
    }
}
